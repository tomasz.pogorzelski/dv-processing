ADD_EXECUTABLE(noise-filtering noise-filtering.cpp)

TARGET_LINK_LIBRARIES(noise-filtering dv::processing CLI11::CLI11)
