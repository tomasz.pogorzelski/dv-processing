#include "../../include/dv-processing/core/frame.hpp"
#include "../../include/dv-processing/kinematics/motion_compensator.hpp"

#include <benchmark/benchmark.h>

#include <random>

static const size_t eventCount     = 100000;
static const size_t iterationCount = 1500;
static const cv::Size resolution(640, 480);

static void bmPixelAccumulatorFrameRate(benchmark::State &state) {
	dv::PixelAccumulator accumulator(resolution);
	dv::EventStore store;

	std::default_random_engine generator;
	std::uniform_real_distribution<double> distribution(0.0, 1.0);

	for (size_t i = 0; i < eventCount; i++) {
		store.emplace_back(0, distribution(generator) * resolution.width, distribution(generator) * resolution.height,
			distribution(generator) > 0.5);
	}

	int64_t counter = 0;
	dv::Frame frame(0, cv::Mat(resolution, CV_8UC1));
	for (auto _ : state) {
		accumulator.accept(store);
		accumulator.generateFrame(frame);
		counter++;
	}
	state.SetItemsProcessed(counter);
	state.SetLabel("frames");
}

static void bmPixelAccumulatorEventRate(benchmark::State &state) {
	dv::PixelAccumulator accumulator(resolution);
	dv::EventStore store;

	std::default_random_engine generator;
	std::uniform_real_distribution<double> distribution(0.0, 1.0);

	for (size_t i = 0; i < eventCount; i++) {
		store.emplace_back(0, distribution(generator) * resolution.width, distribution(generator) * resolution.height,
			distribution(generator) > 0.5);
	}
	size_t counter = 0;
	dv::Frame frame(0, cv::Mat(resolution, CV_8UC1));
	for (auto _ : state) {
		accumulator.accept(store);
		accumulator.generateFrame(frame);
		counter += store.size();
	}
	state.SetItemsProcessed(static_cast<int64_t>(counter));
	state.SetLabel("events");
}

static void bmAccumulatorEventRate(benchmark::State &state) {
	dv::Accumulator accumulator(
		resolution, dv::Accumulator::Decay::EXPONENTIAL, 1e+6, false, 0.03f, 0.3f, 0.0, 0.0, false);
	dv::EventStore store;

	std::default_random_engine generator;
	std::uniform_real_distribution<double> distribution(0.0, 1.0);

	for (size_t i = 0; i < eventCount; i++) {
		store.emplace_back(0, distribution(generator) * resolution.width, distribution(generator) * resolution.height,
			distribution(generator) > 0.5);
	}
	dv::Frame frame(0, cv::Mat(resolution, CV_32FC1));
	size_t counter = 0;
	for (auto _ : state) {
		accumulator.accept(store);
		accumulator.generateFrame(frame);
		counter += store.size();
	}
	state.SetItemsProcessed(static_cast<int64_t>(counter));
	state.SetLabel("events");
}

static void bmAccumulatorFrameRate(benchmark::State &state) {
	dv::Accumulator accumulator(
		resolution, dv::Accumulator::Decay::EXPONENTIAL, 1e+6, false, 0.03f, 0.3f, 0.0, 0.0, false);
	dv::EventStore store;

	std::default_random_engine generator;
	std::uniform_real_distribution<double> distribution(0.0, 1.0);

	for (size_t i = 0; i < eventCount; i++) {
		store.emplace_back(0, distribution(generator) * resolution.width, distribution(generator) * resolution.height,
			distribution(generator) > 0.5);
	}
	dv::Frame frame(0, cv::Mat(resolution, CV_32FC1));
	int64_t counter = 0;
	for (auto _ : state) {
		accumulator.accept(store);
		accumulator.generateFrame(frame);
		counter++;
	}
	state.SetItemsProcessed(counter);
	state.SetLabel("frames");
}

static void bmMotionCompensatorEventRate(benchmark::State &state) {
	dv::kinematics::MotionCompensator accumulator(resolution);
	dv::EventStore store;

	std::default_random_engine generator;
	std::uniform_real_distribution<double> distribution(0.0, 1.0);

	for (size_t i = 0; i < eventCount; i++) {
		store.emplace_back(1000, distribution(generator) * resolution.width,
			distribution(generator) * resolution.height, distribution(generator) > 0.5);
	}

	accumulator.accept(dv::kinematics::Transformationf(500, Eigen::Matrix4f::Identity()));
	accumulator.accept(dv::kinematics::Transformationf(1000, Eigen::Matrix4f::Identity()));
	accumulator.accept(dv::kinematics::Transformationf(1500, Eigen::Matrix4f::Identity()));

	accumulator.accept(dv::measurements::Depth(1000, 3.f));

	size_t counter = 0;
	for (auto _ : state) {
		accumulator.accept(store);
		accumulator.generateFrame();
		counter += store.size();
	}
	state.SetItemsProcessed(static_cast<int64_t>(counter));
	state.SetLabel("events");
}

static void bmMotionCompensatorFrameRate(benchmark::State &state) {
	dv::kinematics::MotionCompensator accumulator(resolution);
	dv::EventStore store;

	std::default_random_engine generator;
	std::uniform_real_distribution<double> distribution(0.0, 1.0);

	for (size_t i = 0; i < eventCount; i++) {
		store.emplace_back(1000, distribution(generator) * resolution.width,
			distribution(generator) * resolution.height, distribution(generator) > 0.5);
	}

	accumulator.accept(dv::kinematics::Transformationf(500, Eigen::Matrix4f::Identity()));
	accumulator.accept(dv::kinematics::Transformationf(1000, Eigen::Matrix4f::Identity()));
	accumulator.accept(dv::kinematics::Transformationf(1500, Eigen::Matrix4f::Identity()));

	accumulator.accept(dv::measurements::Depth(1000, 3.f));

	int64_t counter = 0;
	for (auto _ : state) {
		accumulator.accept(store);
		accumulator.generateFrame();
		counter++;
	}
	state.SetItemsProcessed(counter);
	state.SetLabel("frames");
}

int main(int argc, char **argv) {
	BENCHMARK(bmPixelAccumulatorFrameRate);
	BENCHMARK(bmPixelAccumulatorEventRate);
	BENCHMARK(bmAccumulatorFrameRate);
	BENCHMARK(bmAccumulatorEventRate);
	BENCHMARK(bmMotionCompensatorFrameRate);
	BENCHMARK(bmMotionCompensatorEventRate);

	::benchmark::Initialize(&argc, argv);
	if (::benchmark::ReportUnrecognizedArguments(argc, argv)) {
		return 1;
	}
	::benchmark::RunSpecifiedBenchmarks();

	return EXIT_SUCCESS;
}
