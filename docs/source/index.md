# Welcome to dv-processing's documentation!

```{toctree}
:maxdepth: 2
:caption: Contents

motion_compensation.md
api.md
```

Generic algorithms for event cameras.
