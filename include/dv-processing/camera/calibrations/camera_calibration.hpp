#pragma once

#include "../../external/fmt_compat.hpp"

#include "../../exception/exception.hpp"
#include "../camera_geometry.hpp"

#include <Eigen/Core>
#include <boost/property_tree/ptree.hpp>
#include <opencv2/core.hpp>

#include <optional>

namespace dv::camera::calibrations {

namespace pt = boost::property_tree;

class CameraCalibration {
public:
	class Metadata {
	public:
		/// Shape of the calibration pattern
		cv::Size patternShape;

		/// Shape of the calibration pattern in terms of internal intersections
		cv::Size internalPatternShape;

		/// Type of the calibration pattern used (e.g. apriltag)
		std::string patternType;

		/// Size of the calibration pattern in [m]
		float patternSize = -1.f;

		/// Ratio between tags to patternSize (apriltag only)
		float patternSpacing = -1.f;

		/// Calibration reprojection error
		std::optional<float> calibrationError = std::nullopt;

		/// Timestamp when the calibration was conducted
		std::string calibrationTime;

		/// Description of the calibration quality (excellent/good/bad etc)
		std::string quality;

		/// Any additional information
		std::string comment;

		Metadata() = default;

		Metadata(const cv::Size &patternShape, const cv::Size &internalPatternShape, const std::string &patternType,
			float patternSize, float patternSpacing, const std::optional<float> &calibrationError,
			const std::string &calibrationTime, const std::string &quality, const std::string &comment) :
			patternShape(patternShape),
			internalPatternShape(internalPatternShape),
			patternType(patternType),
			patternSize(patternSize),
			patternSpacing(patternSpacing),
			calibrationError(calibrationError),
			calibrationTime(calibrationTime),
			quality(quality),
			comment(comment) {
		}

		/**
		 * Create an instance of metadata from a property tree structure.
		 * @param tree 		Property tree to be parsed.
		 * @return 			Constructed Metadata instance.
		 */
		explicit Metadata(const pt::ptree &tree) :
			Metadata(parsePair<cv::Size, int>(tree, "patternShape", -1),
				parsePair<cv::Size, int>(tree, "internalPatternShape", -1),
				tree.get_child("patternType").get_value<std::string>(""),
				tree.get_child("patternSize").get_value<float>(-1.f),
				tree.get_child("patternSpacing").get_value<float>(-1.f), std::nullopt,
				tree.get_child("calibrationTime").get_value<std::string>(""),
				tree.get_child("quality").get_value<std::string>(""),
				tree.get_child("comment").get_value<std::string>("")) {
			auto errorVal = tree.get_child_optional("calibrationError");
			if (errorVal.has_value()) {
				calibrationError = errorVal->get_value<float>();
			}
		}

		/**
		 * Serialize the metadata structure into a property tree.
		 * @return		Serialized property tree.
		 */
		[[nodiscard]] pt::ptree toPropertyTree() const {
			pt::ptree tree;
			pushVectorToTree<int>("patternShape", {patternShape.width, patternShape.height}, tree);
			pushVectorToTree<int>(
				"internalPatternShape", {internalPatternShape.width, internalPatternShape.height}, tree);
			tree.put("patternType", patternType);
			tree.put("patternSize", patternSize);
			tree.put("patternSpacing", patternSpacing);
			if (calibrationError.has_value()) {
				tree.put("calibrationError", *calibrationError);
			}
			tree.put("calibrationTime", calibrationTime);
			tree.put("quality", quality);
			tree.put("comment", comment);
			return tree;
		}

		/**
		 * Equality operator.
		 * @param rhs
		 * @return
		 */
		bool operator==(const Metadata &rhs) const {
			return patternShape == rhs.patternShape && internalPatternShape == rhs.internalPatternShape
				   && patternType == rhs.patternType && patternSize == rhs.patternSize
				   && patternSpacing == rhs.patternSpacing && calibrationError == rhs.calibrationError
				   && calibrationTime == rhs.calibrationTime && quality == rhs.quality && comment == rhs.comment;
		}
	};

	/// Camera name (e.g. "DVXplorer_DXA02137")
	std::string name;

	/// Description of the location of the camera in the camera rig (e.g. "left")
	std::string position;

	/// Indicate whether it is the master camera in a multi-camera rig
	bool master = false;

	/// Camera resolution width
	cv::Size resolution;

	/// Intersection of optical axis and image plane
	cv::Point2f principalPoint;

	/// Focal length
	cv::Point2f focalLength;

	/// Distortion coefficients
	std::vector<float> distortion;

	/// Distortion model used (can be an empty string or "radialTangential")
	std::string distortionModel;

	/// Transformation from camera zero to this camera
	std::vector<float> transformationToC0;

	/// Metadata
	std::optional<Metadata> metadata = std::nullopt;

	CameraCalibration() = default;

	/**
	 * Parse a property tree and initialize camera calibration out of it.
	 * @param tree 		Serialized property tree containing camera intrinsics calibration.
	 */
	explicit CameraCalibration(const pt::ptree &tree) :
		CameraCalibration(tree.get_child("name").get_value<std::string>(""),
			tree.get_child("position").get_value<std::string>(""), tree.get_child("master").get_value<bool>(false),
			parsePair<cv::Size, int>(tree, "resolution"), parsePair<cv::Point2f, float>(tree, "principalPoint"),
			parsePair<cv::Point2f, float>(tree, "focalLength"),
			getVectorFromTree<float>("distortionCoefficients", tree),
			tree.get_child("distortionModel").get_value<std::string>(""),
			getVectorFromTree<float>("transformationToC0", tree), getOptionalMetadata<Metadata>(tree, "metadata")) {
		if (transformationToC0.size() != 16) {
			throw dv::exceptions::InvalidArgument<size_t>(
				"Expected size of transformationToC0 is 16", transformationToC0.size());
		}
		if (distortion.empty() && !distortionModel.empty()) {
			throw dv::exceptions::InvalidArgument<std::string>(
				"Found distortion parameters but distortionModel is not set!", distortionModel);
		}
	}

	/**
	 * Construct the camera calibration
	 * @param name_ 				Camera name (e.g. "DVXplorer_DXA02137")
	 * @param position_ 			Description of the location of the camera in the camera rig (e.g. "left")
	 * @param master_				Whether camera was a master camera during calibration
	 * @param resolution_ 			Camera resolution
	 * @param principalPoint_		Principal point
	 * @param focalLength_			Focal length
	 * @param distortion_ 			Distortion coefficients
	 * @param distortionModel_ 		Distortion model used (can be empty string or "radialTangential")
	 * @param transformationToC0_ 	Transformation from camera zero to this camera
	 * @param metadata_ 			Metadata
	 */
	CameraCalibration(const std::string &name_, const std::string &position_, const bool master_,
		const cv::Size &resolution_, const cv::Point2f &principalPoint_, const cv::Point2f &focalLength_,
		const std::vector<float> &distortion_, const std::string &distortionModel_,
		const std::vector<float> transformationToC0_, const std::optional<Metadata> &metadata_) :
		name(name_),
		position(position_),
		master(master_),
		resolution(resolution_),
		principalPoint(principalPoint_),
		focalLength(focalLength_),
		distortion(distortion_),
		distortionModel(distortionModel_),
		transformationToC0(transformationToC0_),
		metadata(metadata_) {
		if (distortionModel == "radialTangential" && (distortion.size() < 4 || distortion.size() > 5)) {
			throw dv::exceptions::InvalidArgument<size_t>(
				"Number of distortion coefficient for \"radialTangential\" should be 4 or 5", distortion.size());
		}
	}

	/**
	 * Serialize the CameraCalibration structure into a property tree.
	 * @return		Serialized property tree.
	 */
	[[nodiscard]] pt::ptree toPropertyTree() const {
		pt::ptree tree;
		tree.put("name", name);
		tree.put("position", position);
		tree.put("master", master);
		pushVectorToTree<int>("resolution", {resolution.width, resolution.height}, tree);
		pushVectorToTree<float>("principalPoint", {principalPoint.x, principalPoint.y}, tree);
		pushVectorToTree<float>("focalLength", {focalLength.x, focalLength.y}, tree);
		pushVectorToTree("distortionCoefficients", distortion, tree);
		tree.put("distortionModel", distortionModel);
		pushVectorToTree("transformationToC0", transformationToC0, tree);
		if (metadata.has_value()) {
			tree.put_child("metadata", metadata->toPropertyTree());
		}
		return tree;
	}

	/**
	 * Return the transformation matrix to C0 as a Eigen matrix.
	 * @return 	Eigen matrix containing transformation to camera "C0".
	 */
	[[nodiscard]] Eigen::Matrix4f getTransformMatrix() const {
		return Eigen::Matrix4f(transformationToC0.data());
	}

	/**
	 * Equality operator for the class, compares each member of the class.
	 * @param rhs 	Other instance of this class
	 * @return
	 */
	[[nodiscard]] bool operator==(const CameraCalibration &rhs) const {
		return master == rhs.master && name == rhs.name && position == rhs.position && resolution == rhs.resolution
			   && principalPoint == rhs.principalPoint && focalLength == rhs.focalLength && distortion == rhs.distortion
			   && distortionModel == rhs.distortionModel && transformationToC0 == rhs.transformationToC0
			   && metadata == rhs.metadata;
	}

	/**
	 * Retrieve camera geometry instance from this calibration instance. Distortion model is going to be
	 * ignored if the `CameraGeometry` class doesn't support the distortion model.
	 *
	 * CameraGeometry class only supports "radialTangential" distortion model.
	 * @return 		Camera geometry class that implements geometrical transformations of pixel coordinates.
	 */
	[[nodiscard]] dv::camera::CameraGeometry getCameraGeometry() const {
		if (distortion.empty() || distortionModel != "radialTangential") {
			return {focalLength.x, focalLength.y, principalPoint.x, principalPoint.y, resolution};
		}
		// TODO(rokas): add distortion model name and parameter count validation when more distortion models are added
		return {distortion, focalLength.x, focalLength.y, principalPoint.x, principalPoint.y, resolution};
	}

	/**
	 * Serialize the object into a stream.
	 * @param os
	 * @param calibration
	 * @return
	 */
	friend std::ostream &operator<<(std::ostream &os, const CameraCalibration &calibration) {
		os << std::boolalpha;
		os << "name: " << calibration.name << std::endl;
		os << "position: " << calibration.position << std::endl;
		os << "master: " << calibration.master << std::endl;
		os << "resolution: " << calibration.resolution << std::endl;
		os << "principalPoint: " << calibration.principalPoint << std::endl;
		os << "focalLength: " << calibration.focalLength << std::endl;
		os << "distortion: " << fmt::format("[{}]", fmt::join(calibration.distortion, ", ")) << std::endl;
		os << "distortionModel: " << calibration.distortionModel << std::endl;
		os << "transformationToC0: " << fmt::format("[{}]", fmt::join(calibration.distortion, ", ")) << std::endl;
		os << std::noboolalpha;
		return os;
	}

protected:
	/**
	 * Push a vector of the given type to the property tree at the given key.
	 */
	template<typename T>
	static void pushVectorToTree(const std::string &key, const std::vector<T> &vals, pt::ptree &tree) {
		pt::ptree row;
		for (const auto &val : vals) {
			pt::ptree cell;
			cell.put_value(val);
			row.push_back(std::make_pair("", cell));
		}
		tree.add_child(key, row);
	}

	/**
	 * Retrieve a vector of the given type from the property tree from the given key.
	 * @return		A sequence value in a std::vector container.
	 */
	template<typename T>
	static std::vector<T> getVectorFromTree(const std::string &key, const pt::ptree &tree) {
		std::vector<T> vec;

		for (const auto &cell : tree.get_child(key)) {
			vec.push_back(cell.second.get_value<T>());
		}

		return vec;
	}

	template<class Container, typename Scalar>
	static Container parsePair(
		const pt::ptree &child, const std::string &name, std::optional<Scalar> defaults = std::nullopt) {
		// If default value is provided and tree doesn't exist, return defaults
		if (defaults.has_value() && !child.get_child_optional(name).has_value()) {
			return {defaults.value(), defaults.value()};
		}

		const auto vec = getVectorFromTree<Scalar>(name, child);
		if (vec.size() != 2) {
			throw dv::exceptions::InvalidArgument<size_t>(
				fmt::format("Expected number of items in {} is 2", name), vec.size());
		}
		return {vec[0], vec[1]};
	}

	template<class Container, typename Scalar>
	static Container parseTripple(
		const pt::ptree &child, const std::string &name, std::optional<Scalar> defaults = std::nullopt) {
		// If default value is provided and tree doesn't exist, return defaults
		if (defaults.has_value() && !child.get_child_optional(name).has_value()) {
			return {defaults.value(), defaults.value(), defaults.value()};
		}

		const auto vec = CameraCalibration::getVectorFromTree<Scalar>(name, child);
		if (vec.size() != 3) {
			throw dv::exceptions::InvalidArgument<size_t>(
				fmt::format("Expected number of items in {} is 3", name), vec.size());
		}
		return {vec[0], vec[1], vec[2]};
	}

	template<class MetadataClass>
	static std::optional<MetadataClass> getOptionalMetadata(
		const boost::property_tree::ptree &tree, const std::string &path) {
		const auto child = tree.get_child_optional(path);
		if (child.has_value()) {
			return MetadataClass(*child);
		}
		else {
			return std::nullopt;
		}
	}

	friend struct IMUCalibration;
	friend struct StereoCalibration;
};

} // namespace dv::camera::calibrations
