#pragma once

#include "../external/concepts_compat.hpp"

#include "../data/bounding_box_base.hpp"
#include "../data/event_base.hpp"
#include "../data/frame_base.hpp"
#include "../data/imu_base.hpp"
#include "../data/pose_base.hpp"
#include "../data/timed_keypoint_base.hpp"
#include "../data/trigger_base.hpp"

#include <Eigen/Core>
#include <boost/callable_traits.hpp>
#include <opencv2/core.hpp>

#include <iterator>

namespace dv {

// Forward declarations for concepts
template<typename T>
class cvector;
struct TimedKeyPoint;
struct Frame;

namespace concepts {

namespace internal {

template<typename T>
struct is_eigen_impl : std::false_type {};

template<typename T, int... Is>
struct is_eigen_impl<Eigen::Matrix<T, Is...>> : std::true_type {};

} // namespace internal

template<typename T>
concept is_iterable = requires(T t, size_t sizeType) {
	{t.begin()};
	{t.end()};
};

template<typename T>
concept is_mutable_iterable = is_iterable<T> and requires(T t, size_t sizeType) {
	{t.emplace_back()};
	{t.push_back(*t.begin())};
	{t.shrink_to_fit()};
	{t.resize(sizeType)};
	{t.reserve(sizeType)};
	{ t.size() } -> std::same_as<size_t>;
	{t.operator[](sizeType)};
};

template<typename Derived>
constexpr bool is_eigen_type_f(const Eigen::EigenBase<Derived> *) {
	return true;
}

template<class T>
using iterable_element_type = typename std::remove_reference_t<decltype(*(std::declval<T>().begin()))>;

template<typename T>
constexpr bool is_eigen_type = internal::is_eigen_impl<T>::value;

template<typename T>
concept number = std::integral<T> || std::floating_point<T>;

template<class T>
concept AddressableEvent = requires(T t) {
	{ t.x() } -> std::signed_integral;
	{ t.y() } -> std::signed_integral;
	{ t.timestamp() } -> std::same_as<int64_t>;
	{ t.polarity() } -> std::same_as<bool>;
};

template<class T>
concept HasElementsVector = requires(const T &t) {
	t.elements;
	is_iterable<decltype(t.elements)>;
};

template<class T>
concept Timestamped = requires {
	// See comments in https://stackoverflow.com/a/63347691
	requires std::same_as<decltype(T::timestamp), int64_t>;
};
static_assert(Timestamped<dv::IMU>);
static_assert(Timestamped<dv::Trigger>);
static_assert(Timestamped<dv::Pose>);
static_assert(Timestamped<dv::Frame>);
static_assert(Timestamped<dv::TimedKeyPoint>);
static_assert(Timestamped<dv::BoundingBox>);

template<class T>
concept TimestampedByAccessor = requires(const T &t) {
	{ t.timestamp() } -> std::same_as<int64>;
};
static_assert(TimestampedByAccessor<dv::Event>);

template<class T>
concept HasTimestampedElementsVector = HasElementsVector<T> and requires(const T &t) {
	requires Timestamped<std::remove_reference_t<decltype(t.elements.front())>>;
};
static_assert(HasTimestampedElementsVector<dv::TriggerPacket>);
static_assert(HasTimestampedElementsVector<dv::IMUPacket>);

template<class T>
concept HasTimestampedElementsVectorByAccessor = HasElementsVector<T> and requires(const T &t) {
	requires TimestampedByAccessor<std::remove_reference_t<decltype(t.elements.front())>>;
};
static_assert(HasTimestampedElementsVectorByAccessor<dv::EventPacket>);

template<class T>
concept Coordinate2DMembers = requires {
	// See comments in https://stackoverflow.com/a/63347691
	requires number<decltype(T::x)>;
	requires number<decltype(T::y)>;
};

template<class T>
concept Coordinate2DAccessors = requires(T t) {
	requires number<std::remove_reference_t<decltype(t.x())>>;
	requires number<std::remove_reference_t<decltype(t.y())>>;
};

template<class T>
concept Coordinate3DMembers = requires {
	requires number<decltype(T::x)>;
	requires number<decltype(T::y)>;
	requires number<decltype(T::z)>;
};

template<class T>
concept Coordinate3DAccessors = requires(T t) {
	requires number<std::remove_reference_t<decltype(t.x())>>;
	requires number<std::remove_reference_t<decltype(t.y())>>;
	requires number<std::remove_reference_t<decltype(t.z())>>;
};

template<class T>
concept Coordinate2D = (Coordinate2DAccessors<T> or Coordinate2DMembers<T>) and(
	!is_eigen_type<T> or (static_cast<bool>(T::IsVectorAtCompileTime) and T::SizeAtCompileTime >= 2));

template<class T>
concept Coordinate3D = (Coordinate3DAccessors<T> or Coordinate3DMembers<T>) and(
	!is_eigen_type<T> or (static_cast<bool>(T::IsVectorAtCompileTime) and T::SizeAtCompileTime >= 3));

template<class T>
concept Coordinate2DCostructible = Coordinate2D<T> and requires {
	{ T(0, 0) } -> std::same_as<T>;
};

template<class T>
concept Coordinate3DCostructible = Coordinate3D<T> and requires {
	{ T(0, 0, 0) } -> std::same_as<T>;
};

template<class T>
concept Coordinate2DIterable
	= is_iterable<T> and Coordinate2D<iterable_element_type<T>> and Coordinate2DCostructible<iterable_element_type<T>>;

template<class T>
concept Coordinate2DMutableIterable = is_mutable_iterable<T> and Coordinate2D<
	iterable_element_type<T>> and Coordinate2DCostructible<iterable_element_type<T>>;

template<class T>
concept Coordinate3DIterable
	= is_iterable<T> and Coordinate3D<iterable_element_type<T>> and Coordinate3DCostructible<iterable_element_type<T>>;

template<class T>
concept Coordinate3DMutableIterable = is_mutable_iterable<T> and Coordinate3D<
	iterable_element_type<T>> and Coordinate3DCostructible<iterable_element_type<T>>;

static_assert(Coordinate2D<cv::Point2i>);
static_assert(Coordinate2D<Eigen::Vector2i>);
static_assert(Coordinate2D<cv::Point2f>);
static_assert(Coordinate2D<Eigen::Vector2f>);
static_assert(Coordinate2D<cv::Point2d>);
static_assert(Coordinate2D<Eigen::Vector2d>);

static_assert(Coordinate2D<cv::Point3i>);
static_assert(Coordinate2D<Eigen::Vector3i>);
static_assert(Coordinate2D<cv::Point3f>);
static_assert(Coordinate2D<Eigen::Vector3f>);
static_assert(Coordinate2D<cv::Point3d>);
static_assert(Coordinate2D<Eigen::Vector3d>);

static_assert(not Coordinate3D<cv::Point2i>);
static_assert(not Coordinate3D<Eigen::Vector2i>);
static_assert(not Coordinate3D<cv::Point2f>);
static_assert(not Coordinate3D<Eigen::Vector2f>);
static_assert(not Coordinate3D<cv::Point2d>);
static_assert(not Coordinate3D<Eigen::Vector2d>);

static_assert(Coordinate3D<cv::Point3i>);
static_assert(Coordinate3D<Eigen::Vector3i>);
static_assert(Coordinate3D<cv::Point3f>);
static_assert(Coordinate3D<Eigen::Vector3f>);
static_assert(Coordinate3D<cv::Point3d>);
static_assert(Coordinate3D<Eigen::Vector3d>);

template<class T>
concept EigenType = is_eigen_type<T>;

template<class T1, class T2>
concept InputStreamableFrom = requires(T1 t1, T2 t2) {
	{ t2 >> t1 } -> std::same_as<T1 &>;
};

template<class T1, class T2>
concept InputStreamableTo = requires(T1 t1, T2 t2) {
	{ t1 >> t2 } -> std::same_as<T2 &>;
};

template<class T1, class T2>
concept Accepts = requires(T1 t1, T2 t2) {
	{t1.accept(t2)};
};

template<class T1, class T2>
concept OutputStreamableFrom = requires(T1 t1, T2 t2) {
	{ t1 << t2 } -> std::same_as<T1 &>;
};

template<class T1, class T2>
concept OutputStreamableTo = requires(T1 t1, T2 t2) {
	{ t2 << t1 } -> std::same_as<T2 &>;
};

template<class T>
concept FrameOutputGenerator = requires(T t) {
	{ t.generateFrame() } -> std::same_as<dv::Frame>;
};

template<class T, class EventStoreType>
concept EventOutputGenerator = requires(T t) {
	{ t.generateEvents() } -> std::same_as<EventStoreType>;
};

template<class T1, class T2>
concept IOStreamableFrom = InputStreamableFrom<T1, T2> && OutputStreamableFrom<T1, T2>;

template<class T1, class T2>
concept IOStreamableTo = InputStreamableTo<T1, T2> && OutputStreamableTo<T1, T2>;

template<class T, class EventStoreType>
concept EventToFrameConverter = OutputStreamableFrom<T, EventStoreType> && Accepts<T,
	EventStoreType> && InputStreamableTo<T, dv::Frame> && FrameOutputGenerator<T>;

template<class T>
concept FrameToFrameConverter = OutputStreamableFrom<T, dv::Frame> && Accepts<T, dv::Frame> && InputStreamableTo<T,
	dv::Frame> && FrameOutputGenerator<T>;

template<class T, class EventStoreType>
concept EventToEventConverter
	= OutputStreamableFrom<T, EventStoreType> && Accepts<T, EventStoreType> && InputStreamableTo<T,
		EventStoreType> && InputStreamableTo<T, EventStoreType> && EventOutputGenerator<T, EventStoreType>;

template<class T, class EventStoreType>
concept FrameToEventConverter = OutputStreamableFrom<T, dv::Frame> && Accepts<T, dv::Frame> && InputStreamableTo<T,
	EventStoreType> && EventOutputGenerator<T, EventStoreType>;

template<class T>
concept BlockAccessible = requires(T t, int16_t i) {
	{t.block(i, i, i, i)};
};

template<class T>
concept TimestampMatrixContainer = requires(T t, int16_t i) {
	{ t(i, i) } -> std::same_as<int64_t &>;
	{ t.at(i, i) } -> std::same_as<int64_t &>;
};

template<class T>
concept TimedImageContainer = requires {
	// See comments in https://stackoverflow.com/a/63347691
	requires std::same_as<decltype(T::image), cv::Mat>;
	requires std::same_as<decltype(T::timestamp), int64_t>;
};

template<class T>
concept SupportsConstantDepth = requires(T t, float d) {
	{t.setConstantDepth(d)};
};

template<class T, class EventStoreType>
concept TimeSurface = TimestampMatrixContainer<T> && BlockAccessible<T> && EventToFrameConverter<T, EventStoreType>;

template<class T, class Input>
concept DVFeatureDetectorAlgorithm = requires(T t, Input i, cv::Rect roi, cv::Mat mask) {
	{ t.detect(i, roi, mask) } -> std::same_as<dv::cvector<dv::TimedKeyPoint>>;
};

template<class T>
concept OpenCVFeatureDetectorAlgorithm = requires(T *t, cv::Mat m, std::vector<cv::KeyPoint> keypoints, cv::Mat mask) {
	{ t->detect(m, keypoints, mask) } -> std::same_as<void>;
};

template<class T, class Input>
concept FeatureDetectorAlgorithm = DVFeatureDetectorAlgorithm<T, Input> || OpenCVFeatureDetectorAlgorithm<T>;

template<class T, class EventStoreType>
concept EventFilter
	= EventToEventConverter<T, EventStoreType> and requires(T t, typename EventStoreType::value_type event) {
	{ t.retain(event) } -> std::same_as<bool>;
};

/**
 * Checks if function is invocable with the given argument types exactly and its
 * return value is the same as the given return type.
 *
 * @tparam FUNC function-like object to check.
 * @tparam RETURN_TYPE required return type.
 * @tparam ARGUMENTS_TYPES required argument types.
 */
template<typename FUNC, typename RETURN_TYPE, typename... ARGUMENTS_TYPES>
concept return_invocable_strong
	= std::regular_invocable<FUNC, ARGUMENTS_TYPES...> && std::same_as<boost::callable_traits::return_type_t<FUNC>,
		RETURN_TYPE> && std::same_as<boost::callable_traits::args_t<FUNC>, std::tuple<ARGUMENTS_TYPES...>>;

/**
 * Checks if function is invocable with the given argument types and its
 * return value is convertible to the given return type.
 *
 * @tparam FUNC function-like object to check.
 * @tparam RETURN_TYPE required return type.
 * @tparam ARGUMENTS_TYPES required argument types.
 */
template<typename FUNC, typename RETURN_TYPE, typename... ARGUMENTS_TYPES>
concept return_invocable_weak = std::regular_invocable<FUNC,
	ARGUMENTS_TYPES...> && std::convertible_to<boost::callable_traits::return_type_t<FUNC>, RETURN_TYPE>;

template<class T>
concept KeyPointVector = is_mutable_iterable<T> and requires(iterable_element_type<T> t) {
	{Coordinate2D<decltype(t.pt)>};
};

} // namespace concepts
} // namespace dv
