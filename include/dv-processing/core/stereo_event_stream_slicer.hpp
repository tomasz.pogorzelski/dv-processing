#pragma once

#include "core.hpp"

namespace dv {

template<class EventStoreType>
class AddressableStereoEventStreamSlicer {
protected:
	std::optional<size_t> minimumEvents     = std::nullopt;
	std::optional<dv::Duration> minimumTime = std::nullopt;
	AddressableEventStreamSlicer<EventStoreType> slicer;
	EventStoreType otherEvents;

public:
	/**
	 * Adds EventStores from the left and right camera. Performs job evaluation immediately.
	 * @param leftEvents the EventStore from left camera.
	 * @param rightEvents the EventStore from right camera.
	 */
	void accept(const EventStoreType &leftEvents, const EventStoreType &rightEvents) {
		// Buffer right events
		otherEvents.add(rightEvents);
		// Add left and evaluate jobs
		slicer.accept(leftEvents);

		// Retain only relevant right-side events
		if (minimumEvents.has_value()) {
			auto store = otherEvents.sliceBack(minimumEvents.value());
			// Check if we have correct amount of time
			if (!minimumTime.has_value() || store.duration() > minimumTime) {
				otherEvents = store;
			}
		}

		// Limit the time
		if (minimumTime.has_value()) {
			int64_t timeLimit = otherEvents.getHighestTime() + 1;
			auto store        = otherEvents.sliceTime(timeLimit - minimumTime->count(), timeLimit);

			// Check if we have correct count of events
			if (!minimumEvents.has_value() || store.size() > minimumEvents) {
				otherEvents = store;
			}
		}
	}

	/**
	 * Perform an action on the stereo stream data every given amount of events.
	 * Event count is evaluated on the left camera stream and according time interval of data
	 * is sliced from the right camera event stream.
	 * Sliced data is passed into the callback function as soon as it arrived, first
	 * argument is left camera events and second is right camera events.
	 * Since right camera events are sliced by the time interval of left camera, the
	 * amount of events on right camera can be different.
	 * @param n 		the interval (in number of events) in which the callback should be called.
	 * @param callback 	the callback function that gets called on the data every interval.
	 * @return			Job identifier
	 * @sa AddressableEventStreamSlicer::doEveryNumberOfEvents
	 */
	int doEveryNumberOfEvents(
		const size_t n, std::function<void(const EventStoreType &, const EventStoreType &)> callback) {
		if (!minimumEvents.has_value() || minimumEvents < n) {
			minimumEvents = n;
		}

		return slicer.doEveryNumberOfEvents(n, [this, callback](const EventStoreType &leftEvents) {
			const auto rightEvents = otherEvents.sliceTime(leftEvents.getLowestTime(), leftEvents.getHighestTime() + 1);
			callback(leftEvents, rightEvents);
		});
	}

	/**
	 * Perform an action on the stereo stream data every given time interval.
	 * Event period is evaluated on the left camera stream and according time interval of data
	 * is sliced from the right camera event stream.
	 * Sliced data is passed into the callback function as soon as it arrived, first
	 * argument is left camera events and second is right camera events.
	 * @param interval 	Time interval to call the callback function. The callback is called
	 * 					based on timestamps of left camera.
	 * @param callback 	Function to be executed
	 * @return 			Job identifier.
	 * @sa AddressableEventStreamSlicer::doEveryTimeInterval
	 */
	int doEveryTimeInterval(
		const dv::Duration interval, std::function<void(const EventStoreType &, const EventStoreType &)> callback) {
		if (!minimumTime.has_value() || minimumTime < interval) {
			minimumTime = interval;
		}

		return slicer.doEveryTimeInterval(interval, [this, callback](const EventStoreType &leftEvents) {
			const auto rightEvents = otherEvents.sliceTime(leftEvents.getLowestTime(), leftEvents.getHighestTime() + 1);
			callback(leftEvents, rightEvents);
		});
	}

	/**
	 * Returns true if the slicer contains the slicejob with the provided id
	 * @param job the id of the slicejob in question
	 * @return true, if the slicer contains the given slicejob
	 */
	bool hasJob(const int job) {
		return slicer.hasJob(job);
	}

	/**
	 * Removes the given job from the list of current jobs.
	 * @param job The job id to be removed
	 */
	void removeJob(const int job) {
		slicer.removeJob(job);
	}
};

using StereoEventStreamSlicer = AddressableStereoEventStreamSlicer<dv::EventStore>;

} // namespace dv
