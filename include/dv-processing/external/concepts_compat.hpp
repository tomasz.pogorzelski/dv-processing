#pragma once

#include <version>

#if __cplusplus > 201703L && __has_include(<concepts>) && defined(__cpp_lib_concepts) && __cpp_lib_concepts >= 202002L
#	include <concepts>
#else

// Reimplementations of basic concepts due to Apple libc++ not having them.
#	include <concepts>
#	include <type_traits>
#	include "llvm_common_reference.hpp"

namespace std {

template<typename From, typename To>
concept convertible_to = std::is_convertible_v<From, To> && requires(From f) {
	static_cast<To>(f);
};

// NOTE: this is not how the official concept behaves, std::is_base_of_v<> is the
// closest approximation but not exactly right in all cases, this needs internal
// compiler support to be properly implemented.
template<typename Derived, typename Base>
concept derived_from
	= std::is_base_of_v<Base, Derived> && std::is_convertible_v<const volatile Derived *, const volatile Base *>;

template<typename T, typename U>
concept common_reference_with
	= std::same_as<std::common_reference_t<T, U>, std::common_reference_t<U, T>> && convertible_to<T,
		std::common_reference_t<T, U>> && convertible_to<U, std::common_reference_t<T, U>>;

template<typename T, typename U>
concept common_with = std::same_as<std::common_type_t<T, U>, std::common_type_t<U, T>> && requires(T t, U u) {
	static_cast<std::common_type_t<T, U>>(t);
	static_cast<std::common_type_t<T, U>>(u);
} && common_reference_with<std::add_lvalue_reference_t<const T>,
	std::add_lvalue_reference_t<
		const U>> && common_reference_with<std::add_lvalue_reference_t<std::common_type_t<T, U>>,
	std::common_reference_t<std::add_lvalue_reference_t<const T>, std::add_lvalue_reference_t<const U>>>;

template<typename T>
concept integral = std::is_integral_v<T>;

template<typename T>
concept signed_integral = integral<T> && std::is_signed_v<T>;

template<typename T>
concept unsigned_integral = integral<T> && !signed_integral<T>;

template<typename T>
concept floating_point = std::is_floating_point_v<T>;

template<typename LHS, typename RHS>
concept assignable_from
	= std::is_lvalue_reference_v<LHS> && std::common_reference_with < const std::remove_reference_t<LHS>
&, const std::remove_reference_t<RHS> & > &&requires(LHS lhs, RHS &&rhs) {
	{ lhs = std::forward<RHS>(rhs) } -> std::same_as<LHS>;
};

/**
 * std::destructible is implemented in libcxx 12 and Apple, it and std::same_as
 * are the only concepts existing.
 * template<typename T>
 * concept destructible = std::is_nothrow_destructible_v<T>;
 */

template<typename T, typename... Args>
concept constructible_from = std::destructible<T> && std::is_constructible_v<T, Args...>;

template<typename T>
concept default_initializable = constructible_from<T> && requires {
	T{};
	(void) ::new T;
};

template<typename T>
concept move_constructible = constructible_from<T, T> && convertible_to<T, T>;

template<typename T>
concept copy_constructible
	= move_constructible<T> && constructible_from<T, T &> && convertible_to<T &, T> && constructible_from<T,
		const T &> && convertible_to<const T &, T> && constructible_from<T, const T> && convertible_to<const T, T>;

// This is a massive simplification, the correct concept uses std::ranges::swap.
// We don't really care about ranges right now.
template<class T>
concept swappable = std::is_swappable_v<T>;

// This is a massive simplification, the correct concept uses std::ranges::swap.
// We don't really care about ranges right now.
template<class T, class U>
concept swappable_with = common_reference_with<T, U> && std::is_swappable_with_v<T, U>;

template<typename T>
concept movable = std::is_object_v<T> && move_constructible<T> && assignable_from<T &, T> && swappable<T>;

template<typename T>
concept copyable = copy_constructible<T> && movable<T> && assignable_from<T &, T &> && assignable_from<T &,
	const T &> && assignable_from<T &, const T>;

template<typename T>
concept semiregular = copyable<T> && default_initializable<T>;

namespace internal_W5QIhvMV {
	template<typename T>
	concept boolean_testable = convertible_to<T, bool> && requires(T && t) {
		{ !std::forward<T>(t) } -> convertible_to<bool>;
	};

	template<typename T, typename U>
	concept weakly_equality_comparable_with
		= requires(const std::remove_reference_t<T> &t, const std::remove_reference_t<U> &u) {
		{ t == u } -> boolean_testable;
		{ t != u } -> boolean_testable;
		{ u == t } -> boolean_testable;
		{ u != t } -> boolean_testable;
	};

	template<typename T, typename U>
	concept partially_ordered_with
		= requires(const std::remove_reference_t<T> &t, const std::remove_reference_t<U> &u) {
		{ t < u } -> boolean_testable;
		{ t > u } -> boolean_testable;
		{ t <= u } -> boolean_testable;
		{ t >= u } -> boolean_testable;
		{ u < t } -> boolean_testable;
		{ u > t } -> boolean_testable;
		{ u <= t } -> boolean_testable;
		{ u >= t } -> boolean_testable;
	};
} // namespace internal_W5QIhvMV

template<typename T>
concept equality_comparable = internal_W5QIhvMV::weakly_equality_comparable_with<T, T>;

template<typename T, typename U>
concept equality_comparable_with
	= equality_comparable<T> && equality_comparable<U> && common_reference_with < const std::remove_reference_t<T>
&, const std::remove_reference_t<U> & > &&equality_comparable<
	   std::common_reference_t<const std::remove_reference_t<T> &, const std::remove_reference_t<U> &>>
											&&internal_W5QIhvMV::weakly_equality_comparable_with<T, U>;

template<typename T>
concept totally_ordered = equality_comparable<T> && internal_W5QIhvMV::partially_ordered_with<T, T>;

template<typename T, typename U>
concept totally_ordered_with
	= totally_ordered<T> && totally_ordered<U> && equality_comparable_with<T, U> && totally_ordered
	  < std::common_reference_t < const std::remove_reference_t<T>
&, const std::remove_reference_t<U> & >> &&internal_W5QIhvMV::partially_ordered_with<T, U>;

template<typename T>
concept regular = semiregular<T> && equality_comparable<T>;

template<typename F, typename... Args>
concept invocable = std::is_invocable_v<F, Args...>;

template<typename F, typename... Args>
concept regular_invocable = invocable<F, Args...>;

template<typename F, typename... Args>
concept predicate
	= regular_invocable<F, Args...> && internal_W5QIhvMV::boolean_testable<std::invoke_result_t<F, Args...>>;

template<typename R, typename T, typename U>
concept relation = predicate<R, T, T> && predicate<R, U, U> && predicate<R, T, U> && predicate<R, U, T>;

template<typename R, typename T, typename U>
concept equivalenceRation = relation<R, T, U>;

template<typename R, typename T, typename U>
concept strict_weak_order = relation<R, T, U>;

} // namespace std

#endif
