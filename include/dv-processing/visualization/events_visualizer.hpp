#pragma once

#include "../core/core.hpp"
#include "colors.hpp"

namespace dv::visualization {

class EventVisualizer {
private:
	cv::Size resolution;

	cv::Vec3b backgroundColor;
	cv::Vec3b positiveColor;
	cv::Vec3b negativeColor;

public:
	explicit EventVisualizer(const cv::Size &resolution) :
		EventVisualizer(resolution, colors::white, colors::iniblue, colors::darkgrey) {
	}

	EventVisualizer(const cv::Size &resolution, const cv::Scalar &backgroundColor, const cv::Scalar &positiveColor,
		const cv::Scalar &negativeColor) :
		resolution(resolution),
		backgroundColor(backgroundColor(0), backgroundColor(1), backgroundColor(2)),
		positiveColor(positiveColor(0), positiveColor(1), positiveColor(2)),
		negativeColor(negativeColor(0), negativeColor(1), negativeColor(2)) {
	}

	[[nodiscard]] cv::Mat generateImage(const dv::EventStore &events) const {
		cv::Mat image(resolution, CV_8UC3, backgroundColor);

		for (const auto &event : events) {
			image.at<cv::Vec3b>(event.y(), event.x()) = event.polarity() ? positiveColor : negativeColor;
		}

		return image;
	}
};

} // namespace dv::visualization
