%global __cmake_in_source_build 1
%global debug_package %{nil}

Summary: Generic algorithms for event cameras
Name:    dv-processing
Version: VERSION_REPLACE
Release: 0%{?dist}
License: ASL 2.0
URL:     https://gitlab.com/inivation/dv/dv-processing/
Vendor:  iniVation AG

Source0: https://release.inivation.com/processing/%{name}-%{version}.tar.gz

BuildRequires: gcc >= 10.0, gcc-c++ >= 10.0, systemd-rpm-macros, cmake >= 3.16, pkgconfig >= 0.29.0, boost-devel >= 1.74, opencv-devel >= 4.2.0, eigen3-devel >= 3.3.9, libcaer-devel >= 3.3.12, fmt-devel >= 7.1.3, lz4-devel, libzstd-devel, python3-devel, python3-numpy
Requires: cmake >= 3.16, pkgconfig >= 0.29.0, boost-devel >= 1.74, opencv-devel >= 4.2.0, eigen3-devel >= 3.3.9, libcaer-devel >= 3.3.12, fmt-devel >= 7.1.3, lz4-devel, libzstd-devel

%description
Generic algorithms for event cameras.

%package python
Summary: Generic algorithms for event cameras (Python bindings)
Requires: boost >= 1.74, opencv >= 4.2.0, libcaer >= 3.3.12, fmt >= 7.1.3, lz4-libs, libzstd, python3, python3-numpy

%description python
Generic algorithms for event cameras (Python bindings).

%prep
%autosetup

%build
%cmake -DENABLE_TESTS=1 -DENABLE_BENCHMARKS=0 -DENABLE_SAMPLES=0 -DENABLE_PYTHON=1
%cmake_build

%install
export QA_RPATHS=$(( 0x0001|0x0010 ))
%cmake_install

%files
%{_includedir}/dv-processing/
%{_libdir}/pkgconfig/
%{_libdir}/cmake/dv-processing/

%files python
%{python3_sitearch}/

%changelog
* Mon Apr 20 2020 iniVation AG <support@inivation.com> - VERSION_REPLACE
- See ChangeLog file in source or GitLab.
