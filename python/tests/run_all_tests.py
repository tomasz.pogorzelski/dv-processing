import unittest

from test_event_store import EventStoreTest
from test_stream_slicer import EventStreamSlicerTest
from test_io import FileIOTest
from test_calibration import CalibrationTests

if __name__ == '__main__':
    unittest.main()
