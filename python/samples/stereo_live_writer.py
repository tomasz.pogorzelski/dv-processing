import datetime
import dv_processing as dv
import cv2 as cv
import argparse

parser = argparse.ArgumentParser(
    description='Show a preview of a stereo event stream from a pair of time-synchronized iniVation cameras.')

args = parser.parse_args()

# Open the cameras
camera = dv.io.StereoCapture("DVXplorer_DXA00093", "DAVIS346_00000499")

leftVis = dv.visualization.EventVisualizer(camera.left.getEventResolution())
rightVis = dv.visualization.EventVisualizer(camera.right.getEventResolution())

# Create the preview windows
cv.namedWindow("Left", cv.WINDOW_NORMAL)
cv.namedWindow("Right", cv.WINDOW_NORMAL)

slicer = dv.StereoEventStreamSlicer()

try:
    writer = dv.io.StereoCameraWriter("./stereo.aedat4", camera)

    print("Start recording")
    while camera.left.isConnected() and camera.right.isConnected():
        leftEvents = camera.left.getNextEventBatch()
        if leftEvents is not None:
            writer.left.writeEvents(leftEvents)

        rightEvents = camera.right.getNextEventBatch()
        if rightEvents is not None:
            writer.right.writeEvents(rightEvents)
except KeyboardInterrupt:
    pass

print("Ending recording")
