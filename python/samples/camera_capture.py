import datetime

import dv_processing as dv
import cv2 as cv
import numpy as np
import argparse

parser = argparse.ArgumentParser(description='Show a preview of an iniVation event camera input.')

args = parser.parse_args()

# Open the camera
camera = dv.io.CameraCapture()

# Check whether frames are available
framesAvailable = camera.isFrameStreamAvailable()

# Initialize event accumulator with the known resolution
acc = dv.Accumulator(camera.getEventResolution())

# Some accumulation parameters
acc.setMaxPotential(1.0)
acc.setEventContribution(0.12)

# Create the preview window
cv.namedWindow("Preview", cv.WINDOW_NORMAL)


def accumulate_events(event_slice):
    """
    Handler method for slicer
    :param event_slice:   Sliced events
    :return:
    """
    acc.accept(event_slice)
    event_frame = acc.generateFrame()
    cv.imshow("Preview", event_frame.image)
    cv.waitKey(2)


# Create an event slicer, this will only be used events only camera
slicer = dv.EventStreamSlicer()
slicer.doEveryTimeInterval(datetime.timedelta(milliseconds=33), accumulate_events)

# start read loop
while True:
    # Get events
    events = camera.getNextEventBatch()

    # If no events arrived yet, continue reading
    if events is None:
        continue

    # If frames available, accumulate and display right away
    if framesAvailable:
        # accumulate events
        acc.accept(events)

        # Retrieve an image
        frame = camera.getNextFrame()

        # If an image has arrived
        if frame is not None:
            # Generate event frame and convert to compatible type with frame output
            accumulatedFrame = acc.generateFrame()
            accumulatedImage = (accumulatedFrame.image * 255).astype(np.uint8)
            # Display both images concatenated
            cv.imshow("Preview", cv.hconcat([frame.image, accumulatedImage]))
            cv.waitKey(5)
    else:
        # With event only camera, pass the data to an accumulator
        slicer.accept(events)
