#include "../../include/dv-processing/camera/calibration_set.hpp"

#include "../external/boost-ut/include/boost/ut.hpp"

#include <fstream>
#include <numeric>

namespace fs = std::filesystem;

dv::camera::calibrations::CameraCalibration::Metadata getTestCameraMetadata() {
	return dv::camera::calibrations::CameraCalibration::Metadata{
		{6, 6}, {6, 6}, "maytag", 0.05f, 0.01f, std::nullopt, "today", "not great but not terrible", "no comment"};
}
dv::camera::calibrations::CameraCalibration getTestCameraCalibration(const std::string &name) {
	return dv::camera::calibrations::CameraCalibration(name, "left", true, {640, 480}, {320, 240}, {10, 12},
		{0.0f, 0.01f, 0.02f, 0.03f}, "radialTangential", std::vector<float>(16, -1), getTestCameraMetadata());
}

dv::camera::calibrations::IMUCalibration::Metadata getTestIMUMetadata() {
	return dv::camera::calibrations::IMUCalibration::Metadata{"just now", "nothing to say"};
}
dv::camera::calibrations::IMUCalibration getTestIMUCalibration(const std::string &name) {
	return dv::camera::calibrations::IMUCalibration{name, 0.1f, 0.2f, cv::Point3f(1, 2, 3), cv::Point3f(4, 5, 6), 0.3f,
		0.4f, 0.5f, 0.6f, 0.7f, 0.8f, 900'000, std::vector<float>(16, -1), getTestIMUMetadata()};
}

dv::camera::calibrations::StereoCalibration getTestStereoMetadata(
	const std::string &leftName, const std::string &rightName) {
	return dv::camera::calibrations::StereoCalibration{leftName, rightName,
		std::vector<float>{1.f, 2.f, 3.f, 4.f, 5.f, 6.f, 7.f, 8.f, 9.f},
		std::vector<float>{9.f, 8.f, 7.f, 6.f, 5.f, 4.f, 3.f, 2.f, 1.f},
		dv::camera::calibrations::StereoCalibration::Metadata(-1.f, "nothing to say")};
}

int main() {
	using namespace boost::ut;
	using namespace dv::camera;

	"non_existant_path"_test = [] {
		expect(throws([] {
			const auto _ = CalibrationSet::LoadFromFile("some(garbage-name.xml");
		}));

		expect(throws([] {
			const auto _ = CalibrationSet::LoadFromFile("some(garbage-name.json");
		}));

		expect(throws([] {
			const auto _ = CalibrationSet::LoadFromFile("some(garbage-name");
		}));
	};

	"camera_calibration_serialize"_test = [] {
		std::stringstream ss;

		// Write
		{
			auto cam  = getTestCameraCalibration("camera");
			auto tree = cam.toPropertyTree();
			pt::write_json(ss, tree);
		}

		// Read
		{
			pt::ptree tree;
			pt::read_json(ss, tree);
			auto cam = calibrations::CameraCalibration(tree);

			expect(eq(getTestCameraCalibration("camera"), cam));
		}
	};

	"camera_calibration_meta_serialize"_test = [] {
		std::stringstream ss;

		// Write
		{
			auto meta = getTestCameraMetadata();
			auto tree = meta.toPropertyTree();
			pt::write_json(ss, tree);
		}

		// Read
		{
			pt::ptree tree;
			pt::read_json(ss, tree);
			auto meta = calibrations::CameraCalibration::Metadata(tree);

			expect(meta.comment != "blah");
		}
	};

	"imu_calibration_serialize"_test = [] {
		std::stringstream ss;

		// Write
		{
			auto imu  = getTestIMUCalibration("test");
			auto tree = imu.toPropertyTree();
			pt::write_json(ss, tree);
		}

		// Read
		{
			pt::ptree tree;
			pt::read_json(ss, tree);
			auto imu = calibrations::IMUCalibration(tree);

			expect(eq(getTestIMUCalibration("test"), imu));
		}
	};

	"imu_calibration_meta_serialize"_test = [] {
		std::stringstream ss;

		// Write
		{
			auto meta = getTestIMUMetadata();
			auto tree = meta.toPropertyTree();
			pt::write_json(ss, tree);
		}

		// Read
		{
			pt::ptree tree;
			pt::read_json(ss, tree);
			auto meta = calibrations::IMUCalibration::Metadata(tree);

			expect(meta.comment != "blah");
		}
	};

	"stereo_meta_serialize"_test = [] {
		std::stringstream ss;

		// Write
		{
			auto meta = getTestStereoMetadata("a", "b");
			auto tree = meta.toPropertyTree();
			pt::write_json(ss, tree);
		}

		// Read
		{
			pt::ptree tree;
			pt::read_json(ss, tree);
			auto meta = calibrations::StereoCalibration(tree);
			expect(meta.metadata.has_value());
			expect(meta.metadata->comment != "blah");
		}
	};
	"rig_to_property_tree"_test = [] {
		std::stringstream ss;

		// Write
		CalibrationSet calibration;
		calibration.addCameraCalibration(getTestCameraCalibration("dvxplorer_1"));
		calibration.addCameraCalibration(getTestCameraCalibration("dvxplorer_2"));

		calibration.addImuCalibration(getTestIMUCalibration("dvxplorer_1"));
		calibration.addImuCalibration(getTestIMUCalibration("dvxplorer_2"));

		calibration.addStereoCalibration(getTestStereoMetadata("dvxplorer_1", "dvxplorer_2"));

		auto tree = calibration.toPropertyTree();

		fs::path filepath = "./test_v2_calibration_file.json";
		std::ofstream outfile(filepath);
		pt::write_json(outfile, calibration.toPropertyTree());

		// Read & check
		CalibrationSet calibrationRead = CalibrationSet::LoadFromFile(filepath);

		expect(eq(tree.get<std::string>("version"), std::string("2.0")));
		expect(calibrationRead.getCameraCalibrationByName("dvxplorer_1").has_value());
		expect(calibrationRead.getCameraCalibrationByName("dvxplorer_2").has_value());
		expect(calibrationRead.getImuCalibrationByName("dvxplorer_1").has_value());
		expect(calibrationRead.getImuCalibrationByName("dvxplorer_2").has_value());
		expect(calibrationRead.getStereoCalibrationByLeftCameraName("dvxplorer_1").has_value());
		expect(calibrationRead.getStereoCalibrationByRightCameraName("dvxplorer_2").has_value());
	};

	"master_filename"_test = [] {
		{
			// Completely wrong
			auto cameraNames = dv::camera::CalibrationSet::parseStereoCameraNames("wrong.xml");
			expect(!cameraNames.has_value());
		}

		{
			// Wrong prefix
			auto cameraNames = dv::camera::CalibrationSet::parseStereoCameraNames(
				"calibration_sterEo_DVXplorer_DXA00093_DVXplorer_DXA00087.xml");
			expect(!cameraNames.has_value());
		}

		{
			// Wrong timestamp separator
			auto cameraNames = dv::camera::CalibrationSet::parseStereoCameraNames(
				"calibration_stereo_DVXplorer_DXA00093_DVXplorer_DXA00087Q2021_02_02_16_47_40.xml");
			expect(!cameraNames.has_value());
		}

		{
			// Wrong cameras separator
			auto cameraNames = dv::camera::CalibrationSet::parseStereoCameraNames(
				"calibration_stereo_DVXplorer_DXA00093_DVXplorerDXA00087-2021_02_02_16_47_40.xml");
			expect(!cameraNames.has_value());
		}

		{
			// Correct
			auto cameraNames = dv::camera::CalibrationSet::parseStereoCameraNames(
				"calibration_stereo_DVXplorer_DXA00093_DVXplorer_DXA00087-2021_02_02_16_47_40.xml");
			expect(cameraNames.has_value());
			expect(eq(cameraNames->first, std::string("DVXplorer_DXA00093")));
			expect(eq(cameraNames->second, std::string("DVXplorer_DXA00087")));
		}

		{
			// Correct without extension
			auto cameraNames = dv::camera::CalibrationSet::parseStereoCameraNames(
				"calibration_stereo_DVXplorer_DXA00093_DVXplorer_DXA00087-2021_02_02_16_47_40");
			expect(cameraNames.has_value());
			expect(eq(cameraNames->first, std::string("DVXplorer_DXA00093")));
			expect(eq(cameraNames->second, std::string("DVXplorer_DXA00087")));
		}

		{
			// Mono camera filename
			auto cameraNames = dv::camera::CalibrationSet::parseStereoCameraNames(
				"calibration_camera_DVXplorer_DXA00080-2021_11_01_14_32_23");
			expect(!cameraNames.has_value());
		}
	};

	"xml_mono_file_to_rig"_test = [] {
		fs::path filename = "./calibration_camera_DVXplorer_DXA00080-2021_11_01_14_32_23.xml";
		auto calibration  = CalibrationSet::LoadFromFile(filename);

		expect(eq(calibration.getCameraList().size(), 1));
		expect(eq(calibration.getImuList().size(), 0));
		expect(eq(calibration.getStereoList().size(), 0));
		expect(calibration.getCameraCalibration("C0").has_value());

		auto cameras = calibration.getCameraList();
		expect(eq(cameras.size(), 1));
		expect(eq(cameras[0], std::string("C0")));

		auto cam = *calibration.getCameraCalibration("C0");
		expect(eq(cam.name, std::string("DVXplorer_DXA00080")));
		expect(eq(cam.position, std::string("unknown")));
		expect(eq(cam.distortionModel, std::string("radialTangential")));
		expect(eq(cam.resolution.width, 640));
		expect(eq(cam.resolution.height, 480));
		expect(eq(cam.principalPoint.x, 278.287537_d));
		expect(eq(cam.principalPoint.y, 243.193054_d));
		expect(eq(cam.focalLength.x, 698.462463_d));
		expect(eq(cam.focalLength.y, 697.592712_d));
		expect(eq(cam.distortion.size(), 5));
		expect(lt(std::abs(cam.distortion.at(0) - -3.3964557788001604e-01f), std::numeric_limits<float>::epsilon()));
		expect(lt(std::abs(cam.distortion.at(1) - 4.2529450906452312e-04f), std::numeric_limits<float>::epsilon()));
		expect(lt(std::abs(cam.distortion.at(2) - 2.7961387689219583e-03f), std::numeric_limits<float>::epsilon()));
		expect(lt(std::abs(cam.distortion.at(3) - 1.4214263083290506e-03f), std::numeric_limits<float>::epsilon()));
		expect(lt(std::abs(cam.distortion.at(4) - 2.8338594947193457e-01f), std::numeric_limits<float>::epsilon()));
		auto transSum = std::accumulate(cam.transformationToC0.begin(), cam.transformationToC0.end(), 0.f);
		expect(eq(transSum, 4));

		dv::camera::CameraGeometry geometry = cam.getCameraGeometry();
		expect(eq(geometry.getCentralPoint().x, 278.287537_d));
		expect(eq(geometry.getCentralPoint().y, 243.193054_d));
		expect(eq(geometry.getFocalLength().x, 698.462463_d));
		expect(eq(geometry.getFocalLength().y, 697.592712_d));
		expect(geometry.isUndistortionAvailable());

		auto meta = cam.metadata;
		expect(meta.has_value());
		expect(eq(meta->patternShape.width, 10));
		expect(eq(meta->patternShape.height, 6));
		expect(eq(meta->internalPatternShape.width, 9));
		expect(eq(meta->internalPatternShape.height, 5));
		expect(eq(meta->patternType, std::string("chessboard")));
		expect(eq(meta->patternSize, 0.075_d));
		expect(lt(*meta->calibrationError - 0.988645f, 0.01f));
		expect(eq(meta->calibrationTime, std::string("Mon Nov  1 14:32:23 2021")));

		// Check the number of lines in the output. In case this fails, please make sure that the test above does not
		// need to be updated.
		std::stringstream ss;
		auto tree = calibration.toPropertyTree();
		pt::write_json(ss, tree);
		auto text  = ss.str();
		auto lines = std::count(text.begin(), text.end(), '\n');
		expect(eq(lines, 65));
	};

	"xml_mono_imu_file_to_rig"_test = [] {
		fs::path filename = "./camera_imu_calibration_DVXplorer_DXA00080-2021_09_08_10_54_37.xml";
		auto calibration  = CalibrationSet::LoadFromFile(filename);

		expect(eq(calibration.getCameraList().size(), 1));
		expect(eq(calibration.getImuList().size(), 1));
		expect(eq(calibration.getStereoList().size(), 0));
		expect(calibration.getCameraCalibration("C0").has_value());
		expect(calibration.getImuCalibration("S0").has_value());

		auto cameras = calibration.getCameraList();
		expect(eq(cameras.size(), 1));
		expect(eq(cameras[0], std::string("C0")));
		auto imus = calibration.getImuList();
		expect(eq(imus.size(), 1));
		expect(eq(imus[0], std::string("S0")));
		auto stereo = calibration.getStereoList();
		expect(stereo.empty());

		{
			auto cam = *calibration.getCameraCalibration("C0");
			expect(eq(cam.name, std::string("DVXplorer_DXA00080")));
			expect(eq(cam.position, std::string("unknown")));
			expect(eq(cam.resolution.width, 640));
			expect(eq(cam.resolution.height, 480));
			expect(eq(cam.principalPoint.x, 280.937_d));
			expect(eq(cam.principalPoint.y, 251.474_d));
			expect(eq(cam.focalLength.x, 536.514_d));
			expect(eq(cam.focalLength.y, 537.617_d));
			expect(eq(cam.distortion.size(), 4));
			auto transSum = std::accumulate(cam.transformationToC0.begin(), cam.transformationToC0.end(), 0.f);
			expect(eq(transSum, 4));
			auto meta = cam.metadata;
			expect(meta.has_value());
			expect(eq(meta->patternShape.width, 6));
			expect(eq(meta->patternShape.height, 6));
			expect(eq(meta->internalPatternShape.width, 6));
			expect(eq(meta->internalPatternShape.height, 6));
			expect(eq(meta->patternType, std::string("aprilTag")));
			expect(eq(meta->patternSize, 0.03_d));
			expect(meta->calibrationError
					   .has_value()); // This value can't be interpreted nicely because the type is inconsistent
			expect(eq(meta->calibrationTime, std::string("Mi 08 Sep 2021 10:54:37")));
		}

		{
			auto imu = *calibration.getImuCalibration("S0");
			expect(eq(imu.name, std::string("DVXplorer_DXA00080")));
			expect(eq(imu.timeOffsetMicros, 2897));
			auto transSum = std::accumulate(imu.transformationToC0.begin(), imu.transformationToC0.end(), 0.f);
			expect(lt(std::abs(transSum - 3.809131f), 0.000001f));
			auto meta = imu.metadata;
			expect(meta.has_value());
			expect(eq(meta->calibrationTime, std::string("Mi 08 Sep 2021 10:54:37")));
			expect(
				eq(meta->comment, std::string("Time offset usage: t_correct = t_imu - offset Mean reprojection error: "
											  "3.23208 Mean acc error: 0.478185 Mean gyroscope error: 0.0742152")));

			const auto imuByName = calibration.getImuCalibrationByName("DVXplorer_DXA00080");
			expect(imuByName.has_value());
			expect(eq(imuByName->name, std::string("DVXplorer_DXA00080")));
		}

		// Check the number of lines in the output. In case this fails, please make sure that the test above does not
		// need to be updated.
		std::stringstream ss;
		auto tree = calibration.toPropertyTree();
		pt::write_json(ss, tree);
		auto text  = ss.str();
		auto lines = std::count(text.begin(), text.end(), '\n');
		expect(eq(lines, 110));
	};

	"xml_stereo_file_to_rig"_test = [] {
		fs::path filename = "./calibration_stereo_DVXplorer_DXA00093_DVXplorer_DXA00087-2021_02_02_16_47_40.xml";
		auto calibration  = CalibrationSet::LoadFromFile(filename);

		expect(eq(calibration.getCameraList().size(), 2));
		expect(eq(calibration.getImuList().size(), 0));
		expect(eq(calibration.getStereoList().size(), 1));
		expect(calibration.getCameraCalibration("C0").has_value());
		expect(calibration.getCameraCalibration("C1").has_value());
		expect(calibration.getStereoCalibration("C0_C1").has_value());

		{
			auto cam = calibration.getCameraCalibration("C0").value();
			expect(eq(cam.name, std::string("DVXplorer_DXA00093")));
			expect(eq(cam.position, std::string("master")));
			expect(eq(cam.resolution.width, 640));
			expect(eq(cam.resolution.height, 480));
			expect(eq(cam.principalPoint.x, 332.835_d));
			expect(eq(cam.principalPoint.y, 236.886_d));
			expect(eq(cam.focalLength.x, 627.219_d));
			expect(eq(cam.focalLength.y, 627.426_d));
			expect(eq(cam.distortion.size(), 5));
			expect(lt(std::fabs(cam.distortion[0] - -0.44763f), 0.00001f));
			expect(lt(std::fabs(cam.distortion[1] - 0.55999f), 0.00001f));
			expect(lt(std::fabs(cam.distortion[2] - -0.0003922f), 0.000001f));
			expect(lt(std::fabs(cam.distortion[3] - 0.001793f), 0.00001f));
			expect(lt(std::fabs(cam.distortion[4] - -0.587216f), 0.00001f));
			auto transSum = std::accumulate(cam.transformationToC0.begin(), cam.transformationToC0.end(), 0.f);
			expect(eq(transSum, 4));
			auto meta = cam.metadata;
			expect(meta.has_value());
			expect(eq(meta->patternShape.width, 6));
			expect(eq(meta->patternShape.height, 9));
			expect(eq(meta->internalPatternShape.width, 6));
			expect(eq(meta->internalPatternShape.height, 9));
			expect(eq(meta->patternType, std::string("asymmetricCirclesGrid")));
			expect(eq(meta->patternSize, 0.04_d));
			expect(lt(*meta->calibrationError - 0.132449f, 0.01f));
			expect(eq(meta->calibrationTime, std::string("Tue Feb  2 16:47:40 2021")));

			auto camByName = calibration.getCameraCalibrationByName("DVXplorer_DXA00093");
			expect(camByName.has_value());
			expect(eq(camByName->name, std::string("DVXplorer_DXA00093")));
		}

		{
			auto cam = *calibration.getCameraCalibration("C1");
			expect(eq(cam.name, std::string("DVXplorer_DXA00087")));
			expect(eq(cam.position, std::string("unknown")));
			expect(eq(cam.resolution.width, 640));
			expect(eq(cam.resolution.height, 480));
			expect(eq(cam.principalPoint.x, 338.444_d));
			expect(eq(cam.principalPoint.y, 237.768_d));
			expect(eq(cam.focalLength.x, 623.866_d));
			expect(eq(cam.focalLength.y, 623.564_d));
			expect(eq(cam.distortion.size(), 5));
			auto transSum = std::accumulate(cam.transformationToC0.begin(), cam.transformationToC0.end(), 0.f);
			expect(eq(transSum, 3.87361475f));
			auto meta = cam.metadata;
			expect(meta.has_value());
			expect(eq(meta->patternShape.width, 6));
			expect(eq(meta->patternShape.height, 9));
			expect(eq(meta->internalPatternShape.width, 6));
			expect(eq(meta->internalPatternShape.height, 9));
			expect(eq(meta->patternType, std::string("asymmetricCirclesGrid")));
			expect(eq(meta->patternSize, 0.04_d));
			expect(lt(*meta->calibrationError - 0.132449, 0.01f));
			expect(eq(meta->calibrationTime, std::string("Tue Feb  2 16:47:40 2021")));

			auto camByName = calibration.getCameraCalibrationByName("DVXplorer_DXA00087");
			expect(camByName.has_value());
			expect(eq(camByName->name, std::string("DVXplorer_DXA00087")));
		}

		{
			auto stereo = calibration.getStereoCalibration("C0_C1").value();
			expect(eq(stereo.leftCameraName, std::string("DVXplorer_DXA00093")));
			expect(eq(stereo.rightCameraName, std::string("DVXplorer_DXA00087")));
			expect(stereo.metadata.has_value());
			expect(stereo.metadata->epipolarError.has_value());
			expect(lt(*stereo.metadata->epipolarError - 4.516152f, 0.01f));
			auto esum = std::accumulate(stereo.essentialMatrix.begin(), stereo.essentialMatrix.end(), 0.f);
			expect(lt(std::abs(esum - -0.001522264f), 0.000001f));
			auto fsum = std::accumulate(stereo.fundamentalMatrix.begin(), stereo.fundamentalMatrix.end(), 0.f);
			expect(lt(std::abs(fsum - 0.9972808724f), 0.000001f));

			auto stereoByLeft = calibration.getStereoCalibrationByLeftCameraName("DVXplorer_DXA00093");
			expect(stereoByLeft.has_value());
			expect(eq(stereoByLeft->leftCameraName, std::string("DVXplorer_DXA00093")));
			auto stereoByRight = calibration.getStereoCalibrationByRightCameraName("DVXplorer_DXA00087");
			expect(stereoByRight.has_value());
			expect(eq(stereoByRight->leftCameraName, std::string("DVXplorer_DXA00093")));
		}

		// Check the number of lines in the output. In case this fails, please make sure that the test above does not
		// need to be updated.
		std::stringstream ss;
		auto tree = calibration.toPropertyTree();
		pt::write_json(ss, tree);
		auto text  = ss.str();
		auto lines = std::count(text.begin(), text.end(), '\n');
		expect(eq(lines, 157));
	};

	"load_sample"_test = [] {
		fs::path filename = "./v2.0.json";
		auto calibration  = CalibrationSet::LoadFromFile(filename);

		expect(eq(calibration.getCameraList().size(), 2));
		expect(eq(calibration.getImuList().size(), 2));
		expect(eq(calibration.getStereoList().size(), 1));

		const auto cam1 = calibration.getCameraCalibration("C0");
		expect(cam1.has_value());
		if (cam1.has_value()) {
			expect(eq(cam1->resolution, cv::Size(640, 480)));
			expect(eq(cam1->focalLength, cv::Point2f(10.f, 12.f)));
			expect(eq(cam1->principalPoint, cv::Point2f(320.f, 240.f)));
			expect(eq(cam1->distortionModel, std::string("radialTangential")));
			expect(eq(cam1->position, std::string("left")));
			expect(eq(cam1->name, std::string("dvxplorer")));
			expect(cam1->master);
			expect(cam1->metadata.has_value());
			expect(cam1->metadata->calibrationError.has_value());
			expect(eq(*cam1->metadata->calibrationError, 1000.f));
		}
		const auto cam2 = calibration.getCameraCalibration("C1");
		expect(cam2.has_value());
		if (cam2.has_value()) {
			expect(eq(cam2->resolution, cv::Size(640, 480)));
			expect(eq(cam2->focalLength, cv::Point2f(10.f, 12.f)));
			expect(eq(cam2->principalPoint, cv::Point2f(320.f, 240.f)));
			expect(eq(cam2->distortionModel, std::string("radialTangential")));
			expect(eq(cam2->name, std::string("dvxplorer")));
			expect(eq(cam2->position, std::string("right")));
			expect(!cam2->master);
			expect(!cam2->metadata.has_value());
		}

		const auto imu1 = calibration.getImuCalibration("S0");
		expect(imu1.has_value());
		if (imu1.has_value()) {
			expect(eq(imu1->name, std::string("IMU_DVXplorer_DXA02137")));
			expect(eq(imu1->omegaMax, 0.1f));
			expect(eq(imu1->accMax, 0.2f));
		}
	};

	return EXIT_SUCCESS;
}
