#include "../../include/dv-processing/core/core.hpp"
#include "../../include/dv-processing/core/frame.hpp"

#include "../external/boost-ut/include/boost/ut.hpp"

#include <chrono>

int main() {
	using namespace boost::ut;

	// Generate some data
	int16_t dimension = 100;
	// Setup sample data
	int64_t timestamp = 1e+6;
	dv::EventStore store;
	for (int16_t y = 0; y < dimension; y++) {
		for (int16_t x = 0; x < dimension; x++) {
			store.emplace_back(timestamp, x, y, false);
		}
	}

	static constexpr float epsilon = std::numeric_limits<float>::epsilon();

	"no_decay"_test = [&] {
		dv::Accumulator accumulator(
			cv::Size(100, 100), dv::Accumulator::NONE, 0.0, true, 0.1f, 1.0f, 0.5f, 0.0f, false);
		dv::Frame frame = accumulator.generateFrame();
		// Mean should be equal to neutral potential
		expect(lt(static_cast<float>(cv::mean(frame.image)[0]) - 0.5f, epsilon));

		for (int i = 1; i < 8; i++) {
			accumulator.accumulate(store);
			accumulator.generateFrame(frame);

			// The expected value should not exceed 1.0f
			float expectedValue = std::max(0.5f - (0.1f * static_cast<float>(i)), 0.f);

			expect(lt(static_cast<float>(cv::mean(frame.image)[0]) - expectedValue, epsilon));
		}

		// Reverse polarity
		dv::EventStore positive;
		for (const auto &event : store) {
			positive.emplace_back(event.timestamp(), event.x(), event.y(), true);
		}

		for (int i = 1; i < 15; i++) {
			accumulator.accumulate(positive);
			accumulator.generateFrame(frame);

			// The expected value should not exceed 1.0f
			float expectedValue = std::min(0.1f * static_cast<float>(i), 1.0f);

			expect(lt(static_cast<float>(cv::mean(frame.image)[0]) - expectedValue, epsilon));
		}

		// No decay should happen here
		accumulator.generateFrame(frame);
		expect(lt(static_cast<float>(cv::mean(frame.image)[0]) - 1.f, epsilon));
		accumulator.generateFrame(frame);
		expect(lt(static_cast<float>(cv::mean(frame.image)[0]) - 1.f, epsilon));
	};

	"exponential_decay"_test = [&] {
		dv::Accumulator accumulator(
			cv::Size(100, 100), dv::Accumulator::EXPONENTIAL, 1.0e+6, true, 1.0f, 1.0f, 0.5f, 0.0f, false);

		dv::Frame frame = accumulator.generateFrame();
		// Mean should be equal to neutral potential

		expect(lt(static_cast<float>(cv::mean(frame.image)[0]) - 0.5f, epsilon));
		accumulator.accumulate(store);
		accumulator.generateFrame(frame);
		double lastValue = cv::mean(frame.image)[0];

		for (int i = 1; i < 10; i++) {
			// Reverse polarity
			dv::EventStore positive;
			// We need at least one event with next timestamp to trigger decay
			positive.emplace_back(timestamp + (i * 1000000), static_cast<int16_t>(0), static_cast<int16_t>(0), false);
			accumulator.accumulate(positive);
			accumulator.generateFrame(frame);

			// The expected value should not exceed 1.0f
			double currentValue = cv::mean(frame.image)[0];
			expect(gt(currentValue, lastValue)) << "Value increases to the neutral potential";
		}
		// The mean value should be quite close to 0.5 neutral value
		expect(lt(cv::mean(frame.image)[0] - 0.5, 0.01));
	};

	"linear_decay"_test = [&] {
		dv::Accumulator accumulator(
			cv::Size(100, 100), dv::Accumulator::LINEAR, 1.0e-6, true, 1.0f, 1.0f, 0.5f, 0.0f, false);

		dv::Frame frame = accumulator.generateFrame();
		// Mean should be equal to neutral potential

		expect(lt(static_cast<float>(cv::mean(frame.image)[0]) - 0.5f, epsilon));
		accumulator.accumulate(store);
		accumulator.generateFrame(frame);
		double lastValue = cv::mean(frame.image)[0];

		for (int i = 1; i < 10; i++) {
			// Reverse polarity
			dv::EventStore positive;
			// We need at least one event with next timestamp to trigger decay
			positive.emplace_back(timestamp + (i * 1000000), static_cast<int16_t>(0), static_cast<int16_t>(0), false);
			accumulator.accumulate(positive);
			accumulator.generateFrame(frame);

			// The expected value should not exceed 1.0f
			double currentValue = cv::mean(frame.image)[0];
			expect(gt(currentValue, lastValue)) << "Value increases to the neutral potential";
		}
		// The mean value should be quite close to 0.5 neutral value
		expect(lt(cv::mean(frame.image)[0] - 0.5, 0.01));
	};

	"accumulator_bad_input"_test = [&] {
		dv::Frame badSize(0, cv::Mat(cv::Size(2, 2), CV_32FC1));
		dv::Frame badType(0, cv::Mat(cv::Size(100, 100), CV_8UC1));
		dv::Frame badChannels(0, cv::Mat(cv::Size(100, 100), CV_32FC3));
		dv::Accumulator accumulator(
			cv::Size(100, 100), dv::Accumulator::NONE, 0.0, true, 0.1f, 1.0f, 0.5f, 0.0f, false);

		accumulator.accumulate(store);
		expect(throws([&] {
			accumulator.generateFrame(badSize);
		}));
		expect(throws([&] {
			accumulator.generateFrame(badType);
		}));
		expect(throws([&] {
			accumulator.generateFrame(badChannels);
		}));
	};

	"accumulator_neutral_potential_setting"_test = [&] {
		dv::Accumulator acc(cv::Size(1, 1));
		acc.setNeutralPotential(0.5f);
		acc.setMaxPotential(1.f);
		acc.setEventContribution(0.f);

		auto frame = acc.generateFrame();
		expect(eq(frame.image.at<float>(0, 0), 0.5f));

		dv::EventStore events;
		events.emplace_back(1000, 0, 0, true);

		// With zero contribution the image should remain the same
		acc.accept(events);
		frame = acc.generateFrame();
		expect(eq(frame.image.at<float>(0, 0), 0.5f));
	};

	return EXIT_SUCCESS;
}
