#include "../../include/dv-processing/core/stereo_event_stream_slicer.hpp"

#include "../external/boost-ut/include/boost/ut.hpp"

int main() {
	using namespace boost::ut;
	using namespace std::chrono_literals;

	"add_jobs"_test = [] {
		dv::StereoEventStreamSlicer slicer;
		int rangeJobId = slicer.doEveryNumberOfEvents(100, [&](const dv::EventStore &, const dv::EventStore &) {
		});

		int timeJobId = slicer.doEveryTimeInterval(100ms, [&](const dv::EventStore &, const dv::EventStore &) {
		});

		expect(slicer.hasJob(rangeJobId));
		expect(slicer.hasJob(timeJobId));
	};

	"run_jobs"_test = [] {
		dv::StereoEventStreamSlicer slicer;
		size_t leftRangeCounter  = 0;
		size_t rightRangeCounter = 0;
		size_t leftTimeCounter   = 0;
		size_t rightTimeCounter  = 0;
		slicer.doEveryNumberOfEvents(100, [&](const dv::EventStore &left, const dv::EventStore &right) {
			leftRangeCounter += left.size();
			rightRangeCounter += right.size();
		});

		slicer.doEveryTimeInterval(100ms, [&](const dv::EventStore &left, const dv::EventStore &right) {
			leftTimeCounter += left.size();
			rightTimeCounter += right.size();
		});

		int64_t timestamp = 1000LL;
		dv::EventStore events;
		size_t eventAmount = 10000;
		for (size_t i = 0; i <= eventAmount; i++) {
			timestamp += 10000LL;
			events.emplace_back(timestamp, static_cast<int16_t>(0), static_cast<int16_t>(0), false);
		}
		slicer.accept(events, events);

		expect(eq(leftRangeCounter, eventAmount));
		expect(eq(rightRangeCounter, eventAmount));
		expect(eq(leftTimeCounter, eventAmount));
		expect(eq(rightTimeCounter, eventAmount));
	};

	"remove_jobs"_test = [&] {
		dv::StereoEventStreamSlicer slicer;
		int rangeJobId = slicer.doEveryNumberOfEvents(100, [&](const dv::EventStore &, const dv::EventStore &) {
		});

		int timeJobId = slicer.doEveryTimeInterval(100ms, [&](const dv::EventStore &, const dv::EventStore &) {
		});
		expect(slicer.hasJob(rangeJobId));
		expect(slicer.hasJob(timeJobId));
		slicer.removeJob(rangeJobId);
		expect(!slicer.hasJob(rangeJobId));
		slicer.removeJob(timeJobId);
		expect(!slicer.hasJob(timeJobId));
	};
}
