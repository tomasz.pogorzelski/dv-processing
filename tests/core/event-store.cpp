#include "../../include/dv-processing/core/core.hpp"
#include "../../include/dv-processing/core/event.hpp"
#include "../../include/dv-processing/data/utilities.hpp"

#include "../external/boost-ut/include/boost/ut.hpp"

static dv::EventStore generateEvents(size_t eventAmount, int64_t timestamp, int64_t timestampIncrement) {
	// Event Store of event over the last one second
	dv::EventStore store;

	// Setup sample data
	for (size_t i = 0; i < eventAmount; i++) {
		store.emplace_back(timestamp, static_cast<int16_t>(0), static_cast<int16_t>(0), false);
		timestamp += timestampIncrement;
	}
	return store;
}

template<class Store>
void sliceBackTest(Store &store) {
	using namespace boost::ut;
	Store slice1       = store.sliceBack(100);
	size_t eventAmount = store.size();

	expect(eq(slice1.size(), 100UL));
	expect(eq(slice1.getHighestTime(), store.getHighestTime()));
	expect(neq(slice1.getLowestTime(), store.getLowestTime()));

	Store slice2 = store.sliceBack((static_cast<size_t>(2 * eventAmount)));

	expect(eq(slice2.size(), store.size()));
	expect(eq(slice2.getHighestTime(), store.getHighestTime()));
	expect(eq(slice2.getLowestTime(), store.getLowestTime()));

	// The slice should be empty at an invalid region
	Store emptySlice = store.sliceBack(0);
	expect(emptySlice.isEmpty());
	expect(eq(emptySlice.getLowestTime(), 0));
	expect(eq(emptySlice.getHighestTime(), 0));
}

template<class Store>
void sliceBySizeTest(Store &store, int64_t timestampIncrement) {
	using namespace boost::ut;
	int64_t startTime  = store.getLowestTime();
	int64_t endTime    = store.getHighestTime();
	size_t eventAmount = store.size();

	// Sanity checking
	expect(eq(startTime, store.front().timestamp()));
	expect(eq(endTime, store.back().timestamp()));

	Store frontSlice = store.slice(0, 10);
	expect(eq(frontSlice.getLowestTime(), startTime));
	expect(eq(frontSlice.getHighestTime(), startTime + (9 * timestampIncrement)));

	expect(throws([&store, eventAmount] {
		Store overflowSlice = store.slice(0, eventAmount * 2);
	})) << "Throws overflow exception";

	Store fullSlice = store.slice(static_cast<size_t>(0));
	expect(eq(fullSlice.size(), store.size()));

	Store backSlice = store.sliceBack(100);
	expect(eq(backSlice.size(), 100UL));
	expect(eq(backSlice.getHighestTime(), store.getHighestTime()));
	expect(eq(backSlice.getLowestTime(), endTime - (99 * timestampIncrement)));
}

int main() {
	using namespace boost::ut;

	size_t eventAmount         = 100000;
	int64_t timestampIncrement = 10000LL;
	int64_t timestamp          = 1000LL;

	"sharding"_test = [] {
		dv::EventStore store;
		store.setShardCapacity(0);
		expect(eq(store.getShardCapacity(), 1));
		store.setShardCapacity(3);
		expect(eq(store.getShardCapacity(), 3));
		store.emplace_back(1000, 0, 0, true);
		store.emplace_back(1100, 0, 0, true);
		store.emplace_back(1100, 0, 0, true);
		store.emplace_back(1100, 0, 0, true);
		store.emplace_back(1100, 0, 0, true);

		// It should contain two data shards
		expect(eq(store.getShardCount(), 2ULL));
		expect(eq(store.size(), 5ULL));
		expect(eq(store.getLowestTime(), 1000LL));
		expect(eq(store.getHighestTime(), 1100LL));

		dv::EventStore store2;
		store2.setShardCapacity(2);
		store2.emplace_back(1200, 0, 0, true);
		store2.emplace_back(1300, 0, 0, true);
		expect(eq(store2.getShardCount(), 1ULL));
		expect(eq(store2.size(), 2ULL));
		expect(eq(store2.getLowestTime(), 1200LL));
		expect(eq(store2.getHighestTime(), 1300LL));

		store.add(store2);
		expect(eq(store.size(), 7ULL));
		expect(eq(store.getShardCount(), 3ULL));
		expect(eq(store.getLowestTime(), 1000LL));
		expect(eq(store.getHighestTime(), 1300LL));

		dv::EventStore largeShardStore;
		largeShardStore.setShardCapacity(10000);

		// This will allocate a new shard with given capacity
		largeShardStore.emplace_back(1000, 0, 0, true);
		largeShardStore.add(store);
		expect(eq(largeShardStore.size(), 8ULL));
		// Now this should merge the data into a single shard to reduce fragmentation
		expect(eq(largeShardStore.getShardCount(), 1ULL));
		expect(eq(largeShardStore.getLowestTime(), 1000LL));
		expect(eq(largeShardStore.getHighestTime(), 1300LL));
	};

	"shard_slicing"_test = [] {
		dv::EventStore store;
		store.emplace_back(1000, 0, 0, true);
		store.emplace_back(1100, 0, 0, true);
		store.emplace_back(1200, 0, 0, true);
		store.emplace_back(1300, 0, 0, true);
		store.emplace_back(1400, 0, 0, true);

		auto slice = store.slice(1, 2);

		dv::EventStore store2;
		store2.emplace_back(1500, 0, 0, true);
		store2.emplace_back(1600, 0, 0, true);

		slice.add(store2);
		expect(eq(slice.size(), 4));
		expect(eq(slice.at(0).timestamp(), 1100));
		expect(eq(slice.at(1).timestamp(), 1200));
		expect(eq(slice.at(2).timestamp(), 1500));
		expect(eq(slice.at(3).timestamp(), 1600));
		expect(eq(slice.getLowestTime(), 1100));
		expect(eq(slice.getHighestTime(), 1600));
	};

	"add_events"_test = [] {
		dv::EventStore store;
		store.emplace_back(1000, 0, 0, true);
		store.emplace_back(1100, 0, 0, true);

		expect(eq(store.size(), 2ULL));
		expect(eq(store.getLowestTime(), 1000LL));
		expect(eq(store.getHighestTime(), 1100LL));

		dv::EventStore store2;
		store2.emplace_back(1200, 0, 0, true);
		store2.emplace_back(1300, 0, 0, true);
		expect(eq(store2.size(), 2ULL));
		expect(eq(store2.getLowestTime(), 1200LL));
		expect(eq(store2.getHighestTime(), 1300LL));

		store.add(store2);
		expect(eq(store.size(), 4ULL));
		expect(eq(store.getLowestTime(), 1000LL));
		expect(eq(store.getHighestTime(), 1300LL));
	};

	"data_partial_merging"_test = [] {
		dv::PartialEventData<dv::Event, dv::EventPacket> shard;
		dv::PartialEventData<dv::Event, dv::EventPacket> anotherShard;

		// Empty shard merging with itself. Technically that's allowed.
		expect(shard.merge(shard));

		shard._unsafe_addEvent(dv::Event(1000, 0, 0, true));
		// Merging non-empty with an empty shard, should be fine.
		expect(shard.merge(anotherShard));

		// Construct the specific case of const referenced data partial and test it's specific properties
		dv::EventPacket packet;
		packet.elements.emplace_back(1000, 0, 0, true);
		dv::PartialEventData<dv::Event, dv::EventPacket> constShard(std::make_shared<const dv::EventPacket>(packet));

		// Const referenced data shard should not have any capacity and should not be able to merge with other shards
		expect(eq(constShard.availableCapacity(), 0ULL));
		expect(!constShard.canStoreMoreEvents());
		expect(!constShard.merge(shard));
	};

	"slice_back"_test = [=] {
		auto store = generateEvents(eventAmount, timestamp, timestampIncrement);
		sliceBackTest(store);
	};

	"slicing_by_size"_test = [=] {
		auto store = generateEvents(eventAmount, timestamp, timestampIncrement);
		expect(eq(store.front().timestamp(), timestamp));
		sliceBySizeTest(store, timestampIncrement);
	};

	"slicing_by_time"_test = [=] {
		auto store        = generateEvents(eventAmount, timestamp, timestampIncrement);
		int64_t startTime = store.getLowestTime();
		int64_t endTime   = store.getHighestTime();

		// End time should slice the last event
		dv::EventStore lastEventSlice = store.sliceTime(endTime);
		expect(eq(lastEventSlice.size(), 1UL));
		expect(eq(lastEventSlice.getLowestTime(), store.getHighestTime()));
		expect(eq(lastEventSlice.getHighestTime(), lastEventSlice.getLowestTime()));

		dv::EventStore emptySlice = store.sliceTime(endTime, startTime);
		expect(emptySlice.isEmpty());

		size_t begin, end;
		dv::EventStore fullSlice = store.sliceTime(startTime, endTime + 1, begin, end);
		expect(eq(fullSlice.getLowestTime(), store.getLowestTime()));
		expect(eq(fullSlice.getHighestTime(), store.getHighestTime()));
		expect(eq(begin, 0UL));
		expect(eq(end, eventAmount));

		dv::EventStore nullSlice = store.sliceTime(endTime + 1000, endTime + 10000, begin, end);
		expect(eq(nullSlice.getLowestTime(), 0LL));
		expect(eq(nullSlice.getHighestTime(), 0LL));
		expect(eq(begin, 0UL));
		expect(eq(end, 0UL));
	};

	"store_operators"_test = [=] {
		auto store      = generateEvents(eventAmount, timestamp, timestampIncrement);
		int64_t endTime = store.getHighestTime();

		// Should create a copy container of the same events
		dv::EventStore copy;
		copy.add(store);

		expect(eq(copy.size(), eventAmount));
		expect(eq(copy.getLowestTime(), store.getLowestTime()));
		expect(eq(copy.getHighestTime(), store.getHighestTime()));

		int64_t myTime = store.getHighestTime() + timestampIncrement;

		// Direct add method call
		copy.emplace_back(myTime, static_cast<int16_t>(0), static_cast<int16_t>(0), true);
		expect(eq(copy.size(), eventAmount + 1));
		expect(eq(copy.getHighestTime(), myTime));

		// Stream operator
		myTime += timestampIncrement;
		copy << dv::Event(myTime, 0, 0, true);
		expect(eq(copy.size(), eventAmount + 2));
		expect(eq(copy.getHighestTime(), myTime));

		// Adding without incrementing the time should throw an exception
		expect(throws([&] {
			copy.emplace_back(myTime - 1, static_cast<int16_t>(0), static_cast<int16_t>(0), true);
		})) << "Throws out of order exception";

		// Highest time should remain the same
		expect(eq(copy.getHighestTime(), myTime));
		expect(eq(copy.size(), eventAmount + 2));

		dv::EventStore slice = copy.sliceTime(myTime);
		expect(eq(slice.size(), 1UL));

		dv::EventStore duplicate = store + copy.sliceTime(endTime + 1);
		expect(eq(duplicate.size(), eventAmount + 2));

		expect(throws([&] {
			store += store;
		})) << "Throws out of order exception";
	};

	"coord_hash"_test = [] {
		uint32_t hash0 = dv::coordinateHash(0, 0);
		uint32_t hash1 = dv::coordinateHash(10, 0);
		uint32_t hash2 = dv::coordinateHash(0, 10);

		expect(eq(hash0, hash0));
		expect(eq(hash1, hash1));
		expect(eq(hash2, hash2));
		expect(neq(hash2, hash0));
		expect(neq(hash2, hash1));

		uint32_t hash4 = dv::coordinateHash(1, 1);
		uint32_t hash5 = dv::coordinateHash(-1, -1);
		expect(neq(hash4, hash5));
	};

	"filtering_and_scaling"_test = [] {
		dv::EventStore events;

		cv::Rect empty = dv::boundingRect(events);
		expect(eq(empty.area(), 0));

		events.emplace_back(0, static_cast<int16_t>(0), static_cast<int16_t>(0), false);
		events.emplace_back(1, static_cast<int16_t>(10), static_cast<int16_t>(0), false);
		events.emplace_back(2, static_cast<int16_t>(12), static_cast<int16_t>(12), false);
		events.emplace_back(3, static_cast<int16_t>(5), static_cast<int16_t>(0), true);
		events.emplace_back(4, static_cast<int16_t>(18), static_cast<int16_t>(21), true);

		cv::Rect roi(10, 10, 20, 20);
		dv::EventStore filtered;
		dv::roiFilter(events, filtered, roi);

		expect(eq(filtered.size(), 2UL));

		cv::Rect bounds = dv::boundingRect(events);
		expect(eq(bounds.x, 0));
		expect(eq(bounds.y, 0));
		expect(eq(bounds.width, 19));
		expect(eq(bounds.height, 22));

		dv::EventStore positives, negatives;
		dv::polarityFilter(events, positives, true);
		expect(eq(positives.size(), 2UL));

		dv::polarityFilter(events, negatives, false);
		expect(eq(negatives.size(), 3UL));

		double scale = 2.0;
		dv::EventStore scaled;
		dv::scale(events, scaled, scale, scale);
		cv::Rect scaledBounds = dv::boundingRect(scaled);
		expect(eq(scaledBounds.width, 10));
		expect(eq(scaledBounds.height, 11));
	};

	"erase_front"_test = [=] {
		auto store = generateEvents(eventAmount, timestamp, timestampIncrement);

		dv::EventStore eventStore = store.slice(static_cast<size_t>(0));
		size_t priorSize          = eventStore.size();
		eventStore.erase(0, 10);
		expect(eq(eventStore.size(), priorSize - 10));
	};

	"erase_front_middle"_test = [=] {
		auto store = generateEvents(eventAmount, timestamp, timestampIncrement);

		dv::EventStore eventStore = store.slice(static_cast<size_t>(0));
		size_t priorSize          = eventStore.size();
		eventStore.erase(2000, 10);
		expect(eq(eventStore.size(), priorSize - 10));
	};

	"erase_back"_test = [=] {
		auto store = generateEvents(eventAmount, timestamp, timestampIncrement);

		dv::EventStore eventStore = store.slice(static_cast<size_t>(0));
		size_t priorSize          = eventStore.size();
		eventStore.erase(990, 10);
		expect(eq(eventStore.size(), priorSize - 10));
	};

	"erase_back_middle"_test = [=] {
		auto store = generateEvents(eventAmount, timestamp, timestampIncrement);

		dv::EventStore eventStore = store.slice(static_cast<size_t>(0));
		size_t priorSize          = eventStore.size();
		eventStore.erase(2990, 10);
		expect(eq(eventStore.size(), priorSize - 10));
	};

	"erase_within_a_packet"_test = [=] {
		auto store = generateEvents(eventAmount, timestamp, timestampIncrement);

		dv::EventStore eventStore = store.slice(static_cast<size_t>(0));
		size_t priorSize          = eventStore.size();
		eventStore.erase(2350, 10);
		expect(eq(eventStore.size(), priorSize - 10));
	};

	"erase_within_two_packets"_test = [=] {
		auto store = generateEvents(eventAmount, timestamp, timestampIncrement);

		dv::EventStore eventStore = store.slice(static_cast<size_t>(0));
		size_t priorSize          = eventStore.size();
		eventStore.erase(0, 1500);
		expect(eq(eventStore.size(), priorSize - 1500));
	};

	"erase_within_two_packets"_test = [=] {
		auto store = generateEvents(eventAmount, timestamp, timestampIncrement);

		dv::EventStore eventStore = store.slice(static_cast<size_t>(0));
		size_t priorSize          = eventStore.size();
		eventStore.erase(0, 5500);
		expect(eq(eventStore.size(), priorSize - 5500));
	};

	"erase_within_large_range"_test = [=] {
		auto store = generateEvents(eventAmount, timestamp, timestampIncrement);

		dv::EventStore eventStore = store.slice(static_cast<size_t>(0));
		size_t priorSize          = eventStore.size();
		eventStore.erase(1500, 8000);
		expect(eq(eventStore.size(), priorSize - 8000));
	};

	"erase_nothing"_test = [=] {
		auto store = generateEvents(eventAmount, timestamp, timestampIncrement);

		dv::EventStore eventStore = store.slice(static_cast<size_t>(0));
		size_t priorSize          = eventStore.size();
		eventStore.erase(1000, 0);
		expect(eq(eventStore.size(), priorSize));
	};

	"erase_everything"_test = [=] {
		auto store = generateEvents(eventAmount, timestamp, timestampIncrement);

		dv::EventStore eventStore = store.slice(static_cast<size_t>(0));
		size_t priorSize          = eventStore.size();
		eventStore.erase(0, priorSize);
		expect(eq(eventStore.size(), static_cast<size_t>(0)));
	};

	"erase_out_of_range"_test = [=] {
		auto store = generateEvents(eventAmount, timestamp, timestampIncrement);

		dv::EventStore eventStore = store.slice(static_cast<size_t>(0));
		size_t priorSize          = eventStore.size();

		expect(throws([&eventStore, priorSize] {
			eventStore.erase(1500, priorSize);
		}));

		expect(throws([&eventStore, priorSize] {
			eventStore.erase(priorSize + 1500, priorSize);
		}));

		expect(throws([&eventStore, priorSize] {
			eventStore.erase(priorSize + 1500, 0);
		}));
	};

	"erase_time_last"_test = [=] {
		auto store = generateEvents(eventAmount, timestamp, timestampIncrement);

		dv::EventStore eventStore = store.slice(static_cast<size_t>(0));
		eventStore.eraseTime(eventStore.getLowestTime(), eventStore.getHighestTime());
		expect(eq(eventStore.size(), static_cast<size_t>(1)));
	};

	"erase_time_everything"_test = [=] {
		auto store = generateEvents(eventAmount, timestamp, timestampIncrement);

		dv::EventStore eventStore = store.slice(static_cast<size_t>(0));
		eventStore.eraseTime(eventStore.getLowestTime(), eventStore.getHighestTime() + 1);
		expect(eq(eventStore.size(), static_cast<size_t>(0)));
	};

	"erase_time_first_and_last"_test = [=] {
		auto store = generateEvents(eventAmount, timestamp, timestampIncrement);

		dv::EventStore eventStore = store.slice(static_cast<size_t>(0));
		eventStore.eraseTime(eventStore.getLowestTime() + 1, eventStore.getHighestTime());
		expect(eq(eventStore.size(), static_cast<size_t>(2)));
		expect(eq(eventStore.getLowestTime(), store.getLowestTime()));
		expect(eq(eventStore.getHighestTime(), store.getHighestTime()));
		expect(eq(eventStore.at(0).timestamp(), store.getLowestTime()));
		expect(eq(eventStore.at(1).timestamp(), store.getHighestTime()));
	};

	"erase_time_half"_test = [=] {
		auto store = generateEvents(eventAmount, timestamp, timestampIncrement);

		dv::EventStore eventStore = store.slice(static_cast<size_t>(0));
		eventStore.eraseTime(
			eventStore.getLowestTime(), (eventStore.getLowestTime() + eventStore.getHighestTime()) / 2);
		expect(eq(eventStore.size(), store.size() / 2));
		expect(eq(eventStore.at(0).timestamp(), store.at(store.size() / 2).timestamp()));
	};

	"event_at_out_of_bounds"_test = [=] {
		auto store = generateEvents(eventAmount, timestamp, timestampIncrement);

		expect(throws([&] {
			dv::Event ev = store.at(eventAmount);
			// Suppress unused warning
			(void) ev;
		}));
	};

	"retain_duration"_test = [=] {
		auto store = generateEvents(eventAmount, timestamp, timestampIncrement);

		dv::EventStore eventStore = store.slice(static_cast<size_t>(0));
		dv::Duration duration     = store.duration();
		eventStore.retainDuration(duration / 2);
		expect(lt(eventStore.duration().count(), store.duration().count()));
		expect(ge(eventStore.duration().count(), (duration / 2).count()));
	};

	"retain_duration_full"_test = [=] {
		auto store = generateEvents(eventAmount, timestamp, timestampIncrement);

		dv::EventStore eventStore = store.slice(static_cast<size_t>(0));
		dv::Duration duration     = store.duration();
		eventStore.retainDuration(duration);
		expect(eq(eventStore.duration().count(), store.duration().count()));
		// Even with zero duration, it should maintain some events
		eventStore.retainDuration(dv::Duration(0));
		expect(!eventStore.isEmpty());
		expect(le(eventStore.size(), store.size()));
	};

	"timestamp_check"_test = [=] {
		auto store = generateEvents(eventAmount, timestamp, timestampIncrement);

		expect(store.isWithinStoreTimeRange(store.getLowestTime()));
		expect(store.isWithinStoreTimeRange(store.getHighestTime()));
		expect(store.isWithinStoreTimeRange((store.getHighestTime() + store.getLowestTime()) / 2));
		expect(!store.isWithinStoreTimeRange(store.getHighestTime() + 100));
		expect(!store.isWithinStoreTimeRange(store.getLowestTime() - 100));
	};

	"emplace_back"_test = [=] {
		// Event Store of event over the last one second
		dv::EventStore store;

		int16_t position = 0;
		dv::Event event(1000LL, position, position, false);
		store.push_back(event);
		expect(eq(store.size(), 1ULL));

		store.emplace_back(1100LL, position, position, false);
		expect(eq(store.size(), 2ULL));
		expect(eq(store.getLowestTime(), 1000LL));
		expect(eq(store.getHighestTime(), 1100LL));

		dv::AddressableEventStorage<dv::DepthEvent, dv::DepthEventPacket> depthEventStore;
		uint16_t depthValue = 1000;
		depthEventStore.emplace_back(1000LL, position, position, false, depthValue);
		depthEventStore.emplace_back(1100LL, position, position, false, depthValue);
		expect(eq(depthEventStore.size(), 2ULL));
		expect(eq(depthEventStore.getLowestTime(), 1000LL));
		expect(eq(depthEventStore.getHighestTime(), 1100LL));
	};

	"event_rate"_test = [] {
		dv::EventStore store;

		expect(eq(store.rate(), 0.0_d));

		store.emplace_back(100'000, 0, 0, true);
		store.emplace_back(120'000, 0, 0, true);
		store.emplace_back(140'000, 0, 0, true);
		store.emplace_back(160'000, 0, 0, true);
		store.emplace_back(180'000, 0, 0, true);
		store.emplace_back(200'000, 0, 0, true);

		expect(eq(store.rate(), 60.0_d));
	};

	return EXIT_SUCCESS;
}
