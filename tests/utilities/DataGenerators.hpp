#pragma once

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

#include <random>

namespace dv::test {

cv::Mat generateImage(const cv::Size &resolution) {
	cv::Mat image = cv::Mat::zeros(resolution, CV_8UC1);
	cv::Point point(resolution.width / 10, resolution.height / 10);
	cv::rectangle(image, point, point + point, cv::Scalar(128), 5);
	point *= 2;
	cv::rectangle(image, point, point + point, cv::Scalar(128), 5);
	point += point;
	cv::rectangle(image, point, point + point, cv::Scalar(128), 5);
	return image;
}

dv::EventStore generateEventLine(const int64_t timestamp, const cv::Point &a, const cv::Point &b, size_t steps = 0) {
	dv::EventStore store;
	if (steps == 0) {
		steps = static_cast<size_t>(cv::norm(b - a));
	}
	auto totalSteps = static_cast<double>(steps);
	double diffX    = b.x - a.x;
	double diffY    = b.y - a.y;
	for (size_t i = 0; i < steps; i++) {
		double alpha = static_cast<double>(i) / totalSteps;
		store.emplace_back(timestamp, a.x + (alpha * diffX), a.y + (alpha * diffY), true);
	}
	return store;
}

dv::EventStore generateEventRectangle(const int64_t timestamp, const cv::Point &tl, const cv::Point &br) {
	dv::EventStore rectangle;
	// Generate an approximate amount of pixels for a full line
	rectangle.add(generateEventLine(timestamp, tl, cv::Point(tl.x, br.y)));
	rectangle.add(generateEventLine(timestamp, cv::Point(tl.x, br.y), br));
	rectangle.add(generateEventLine(timestamp, br, cv::Point(br.x, tl.y)));
	rectangle.add(generateEventLine(timestamp, cv::Point(br.x, tl.y), tl));
	return rectangle;
}

dv::EventStore generateEventTestSet(const int64_t timestamp, const cv::Size &resolution) {
	dv::EventStore points;
	cv::Point offset(1, 1);
	cv::Point point(resolution.width / 10, resolution.height / 10);
	points.add(generateEventRectangle(timestamp, point, point + point));
	points.add(generateEventRectangle(timestamp, point + offset, (point + point)));
	points.add(generateEventRectangle(timestamp, point - offset, (point + point)));
	point *= 2;
	points.add(generateEventRectangle(timestamp, point, point + point));
	points.add(generateEventRectangle(timestamp, point + offset, (point + point)));
	points.add(generateEventRectangle(timestamp, point - offset, (point + point)));
	point += point;
	points.add(generateEventRectangle(timestamp, point, point + point));
	points.add(generateEventRectangle(timestamp, point + offset, (point + point)));
	points.add(generateEventRectangle(timestamp, point - offset, (point + point)));
	return points;
}

dv::EventStore generateUniformDistEvents(
	const int64_t timestamp, const cv::Size &resolution, const size_t count, const uint64_t seed) {
	std::default_random_engine generator(seed);
	std::uniform_real_distribution<double> distribution(0.0, 1.0);

	dv::EventStore store;
	for (int i = 0; i < count; i++) {
		store.emplace_back(timestamp, static_cast<int16_t>(distribution(generator) * resolution.width),
			static_cast<int16_t>(distribution(generator) * resolution.height), distribution(generator) > 0.5);
	}
	return store;
}

} // namespace dv::test
