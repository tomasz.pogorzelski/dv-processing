//
// Created by rokas on 28.07.21.
//

#include "../../include/dv-processing/kinematics/transformation.hpp"

#include "../external/boost-ut/include/boost/ut.hpp"

#include <Eigen/Geometry>

int main() {
	using namespace boost::ut;
	using namespace dv::kinematics;

	// Test build-up
	Eigen::Quaternionf quaternion(1.f, 0.f, 0.f, 0.f);
	Eigen::Vector3f point(1.f, 2.f, 3.f);
	Transformationf transformation(0, point, quaternion);

	"sanity_checks"_test = [&] {
		// Sanity check whether values are correct
		auto pointCV = transformation.getTranslation<cv::Point3f>();
		expect(eq(point.x(), pointCV.x));
		expect(eq(point.y(), pointCV.y));
		expect(eq(point.z(), pointCV.z));

		// Sanity check 2.
		Eigen::Vector3f pointEigen = transformation.getTranslation();
		expect(eq(point.x(), pointEigen.x()));
		expect(eq(point.y(), pointEigen.y()));
		expect(eq(point.z(), pointEigen.z()));
	};

	"point_transformation"_test = [&] {
		// Transformation against the same point without rotation should yield the coordinate addition
		Eigen::Vector3f transformedPoint = transformation.transformPoint(point);
		expect(eq(point.x() * 2.f, transformedPoint.x()));
		expect(eq(point.y() * 2.f, transformedPoint.y()));
		expect(eq(point.z() * 2.f, transformedPoint.z()));

		// No rotation should return the same point
		Eigen::Vector3f rotatedPoint = transformation.rotatePoint(point);
		expect(eq(point.x(), rotatedPoint.x()));
		expect(eq(point.y(), rotatedPoint.y()));
		expect(eq(point.z(), rotatedPoint.z()));
	};

	"rotation"_test = [point] {
		// Initialize a matrix with a permutation rotation with no translation
		Eigen::Matrix4f transform;
		transform.setZero();
		transform(1, 0) = 1.f;
		transform(0, 2) = 1.f;
		transform(2, 1) = 1.f;
		transform(3, 3) = 1.f;
		Transformationf permutation(0, transform);
		Eigen::Vector3f permuted = permutation.transformPoint(point);
		expect(eq(point.x(), permuted.y()));
		expect(eq(point.y(), permuted.z()));
		expect(eq(point.z(), permuted.x()));
	};

	return EXIT_SUCCESS;
}
